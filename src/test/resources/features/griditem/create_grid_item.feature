Feature: Create grid item
  Creates a new grid item

  Scenario: Creates a new grid item successfully
    Given grid item i is "1"
    And grid item x is 10
    And grid item y is 20
    And grid item w is 100
    And grid item h is 200

    When I create a grid item

    Then I should get the grid item given above with response code 201 "Ok"

  Scenario: Grid item creation fails due to missing mandatory parameters
    When I create a grid item

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | i |
      | x |
      | y |
      | w |
      | h |
