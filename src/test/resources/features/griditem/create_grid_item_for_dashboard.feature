Feature: Create grid item for dashboard
  Creates a grid item for dashboard

  Background: Dashboard is created
    Given dashboard's name is "dashboard1"
    And dashboard's theme is "theme"
    And I create a new dashboard

  Scenario: Creates a new grid item for dashboard successfully
    Given grid item i is "1"
    And grid item x is 10
    And grid item y is 20
    And grid item w is 100
    And grid item h is 200

    When I create a new grid item for the given dashboard
    Then I should get the grid item given above with response code 201 "Created"

    When I read the given dashboard
    Then I should get the dashboard with added grid item with response code 200 "Ok"

  Scenario: Grid item creation fails for non-existent dashboard
    Given grid item i is "1"
    And grid item x is 10
    And grid item y is 20
    And grid item w is 100
    And grid item h is 200

    When I create a new grid item for dashboard with id 0

    Then I should get a response with response code 404 "Not found"



