Feature: Create selected filter for chart
  Creates a selected filter for chart

  Background: Chart is created
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000
    And I create a new chart

  Scenario: Creates a selected filter for chart successfully
    Given selected filter's measureOrDimensionName is "measure"
    And selected filter's measureOrDimensionExpression is "expression"
    And selected filter's type is "measure"
    And selected filter's aggregationFunction is "SUM"
    And selected filter's filterExpression is ">10"
    And selected filter's filterInputType is "input"
    And selected filter's filterDataScopeType is "all"
    And selected filter's allowedDimensionFilterInputTypes is ""

    When I create a new selected filter for the given chart
    Then I should get the selected filter given above with response code 201 "Created"

    When I read the given chart
    Then I should get the chart with added selected filter with response code 200 "Ok"

  Scenario: Selected filter creation fails for non-existent chart
    Given selected filter's measureOrDimensionName is "measure"
    And selected filter's measureOrDimensionExpression is "expression"
    And selected filter's type is "measure"
    And selected filter's aggregationFunction is "SUM"
    And selected filter's filterExpression is ">10"
    And selected filter's filterInputType is "input"
    And selected filter's filterDataScopeType is "all"
    And selected filter's allowedDimensionFilterInputTypes is ""

    When I create a new selected filter for chart with id 0

    Then I should get a response with response code 404 "Not found"



