Feature: Create selected filter
  Creates a new selected filter

  Scenario: Creates a new selected filter successfully
    Given selected filter's measureOrDimensionName is "measure"
    And selected filter's measureOrDimensionExpression is "expression"
    And selected filter's type is "measure"
    And selected filter's aggregationFunction is "SUM"
    And selected filter's filterExpression is ">10"
    And selected filter's filterInputType is "input"
    And selected filter's filterDataScopeType is "all"
    And selected filter's allowedDimensionFilterInputTypes is ""

    When I create a selected filter

    Then I should get the selected filter given above with response code 201 "Ok"

  Scenario: Selected filter creation fails due to missing mandatory parameters
    When I create a selected filter

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | measureOrDimensionName           |
      | measureOrDimensionExpression     |
      | type                             |
      | aggregationFunction              |
      | filterExpression                 |
      | filterInputType                  |
      | filterDataScopeType              |
      | allowedDimensionFilterInputTypes |

