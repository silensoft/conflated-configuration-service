Feature: Create selected dimension for chart
  Creates a selected dimension for chart

  Background: Chart is created
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000
    And I create a new chart

  Scenario: Creates a selected dimension for chart successfully
    Given selected dimension's name is "measure"
    And selected dimension's expression is "expression"
    And selected dimension's isTimestamp is false
    And selected dimension's isDate is false
    And selected dimensions's isString is false
    And selected dimension's visualizationType is "Legend"

    When I create a new selected dimension for the given chart
    Then I should get the selected dimension given above with response code 201 "Created"

    When I read the given chart
    Then I should get the chart with added selected dimension with response code 200 "Ok"

  Scenario: Selected dimension creation fails for non-existent chart
    Given selected dimension's name is "measure"
    And selected dimension's expression is "expression"
    And selected dimension's isTimestamp is false
    And selected dimension's isDate is false
    And selected dimensions's isString is false
    And selected dimension's visualizationType is "Legend"

    When I create a new selected dimension for chart with id 0

    Then I should get a response with response code 404 "Not found"



