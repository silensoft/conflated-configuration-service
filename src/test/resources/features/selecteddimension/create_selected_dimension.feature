Feature: Create selected dimension
  Creates a new selected dimension

  Scenario: Creates a new selected dimension successfully
    Given selected dimension's name is "measure"
    And selected dimension's expression is "expression"
    And selected dimension's isTimestamp is false
    And selected dimension's isDate is false
    And selected dimensions's isString is false
    And selected dimension's visualizationType is "Legend"

    When I create a selected dimension

    Then I should get the selected dimension given above with response code 201 "Ok"

  Scenario: Selected dimension creation fails due to missing mandatory parameters
    When I create a selected dimension

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name              |
      | expression        |
      | isTimestamp       |
      | isDate            |
      | isString          |
      | visualizationType |

