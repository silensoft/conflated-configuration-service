Feature: Create chart for dashboard
  Creates a new chart for dashboard

  Background: Dashboard is created
    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"
    And I create a new dashboard

  Scenario: Creates a new chart for dashboard successfully
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000

    When I create a new chart for the given dashboard
    Then I should get the chart given above with response code 201 "Created"

    When I read the given dashboard
    Then I should get the dashboard with added chart with response code 200 "Ok"

  Scenario: Chart creation fails for non-existent dashboard
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000

    When I create a new chart for dashboard with id 0

    Then I should get a response with response code 404 "Not found"



