Feature: Create chart
  Creates a new chart

  Scenario: Creates a new chart successfully
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000

    When I create a new chart

    Then I should get the chart given above with response code 201 "Created"

  Scenario: Chart creation fails due to missing mandatory parameter
    When I create a new chart

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | layoutId                  |
      | fetchedRowCount           |
      | xAxisCategoriesShownCount |
      | chartType                 |