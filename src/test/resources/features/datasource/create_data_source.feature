Feature: Create data source
  Creates a new data source

  Scenario: Creates a new data source successfully
    Given data source's name is "data source"
    And data source's jdbcDriverClass is "org.jdbc.driver"
    And data source's jdbcUrl is "jdbc url"
    And data source's userName is "user name"
    And data source's password is "passwd"
    And data source's sqlStatement is "SELECT FROM"
    And data source's dataRefreshIntervalValue is 15
    And data source's dataRefreshIntervalUnit is "Minutes"
    And data sources dataRefreshDelayValue is 60
    And data source's dataRefreshDelayUnit is "Seconds"

    When I create a new data source

    Then I should get the data source given above with response code 201 "Created"

  Scenario: Data source creation fails due to missing mandatory parameters
    When I create a new data source

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name                     |
      | jdbcDriverClass          |
      | jdbcUrl                  |
      | userName                 |
      | password                 |
      | sqlStatement             |
      | dataRefreshIntervalValue |
      | dataRefreshIntervalUnit  |
      | dataRefreshDelayValue    |
      | dataRefreshDelayUnit     |

