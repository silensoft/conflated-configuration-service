Feature: Create filter for data source
  Creates a new filter for data source

  Background: Data source is created
    Given data source's name is "data source"
    And data source's jdbcDriverClass is "org.jdbc.driver"
    And data source's jdbcUrl is "jdbc url"
    And data source's userName is "user name"
    And data source's password is "passwd"
    And data source's sqlStatement is "SELECT FROM"
    And data source's dataRefreshIntervalValue is 15
    And data source's dataRefreshIntervalUnit is "Minutes"
    And data sources dataRefreshDelayValue is 60
    And data source's dataRefreshDelayUnit is "Seconds"
    And I create a new data source

  Scenario: Creates a new filter for data source successfully
    Given filter name is "filter"
    And filter expression is "expression"
    And filter unit is ""
    And filter type is "measure"

    When I create a new filter for the given data source
    Then I should get the filter given above with response code 201 "Created"

    When I read the given data source
    Then I should get the data source with added filter with response code 200 "Ok"

  Scenario: Filter creation fails for non-existent data source
    Given filter name is "filter"
    And filter expression is "expression"
    And filter unit is ""
    And filter type is "measure"

    When I create a new filter for data source with id 0

    Then I should get a response with response code 404 "Not found"



