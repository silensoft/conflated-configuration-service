Feature: Create filter
  Creates a new filter

  Scenario: Creates a new filter successfully
    Given filter name is "filter"
    And filter expression is "expression"
    And filter unit is ""
    And filter type is "measure"

    When I create a filter

    Then I should get the filter given above with response code 201 "Ok"

  Scenario: Filter creation fails due to missing mandatory parameters
    When I create a filter

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name        |
      | expression  |
      | unit        |
      | type        |
