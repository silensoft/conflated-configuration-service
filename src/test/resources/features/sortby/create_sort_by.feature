Feature: Create sort by
  Creates a new sort by

  Scenario: Creates a new sort by successfully
    Given sort by's name is "name"
    And sort by's expression is "expression"
    And sort by's type is "measure"

    When I create a sort by

    Then I should get the sort by given above with response code 201 "Ok"

  Scenario: Sort by creation fails due to missing mandatory parameters
    When I create a sort by

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name       |
      | expression |
      | type       |



