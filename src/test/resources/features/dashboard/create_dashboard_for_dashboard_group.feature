Feature: Create dashboard for dashboard group
  Creates a new dashboard for dashboard group

  Background: Dashboard group is created
    Given dashboard group's name is "dashboard group"
    And I create a new dashboard group

  Scenario: Creates a new dashboard for dashboard group successfully
    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"

    When I create a new dashboard for the given dashboard group
    Then I should get the dashboard given above with response code 201 "Created"

    When I read the given dashboard group
    Then I should get the dashboard group with added dashboard with response code 200 "Ok"

  Scenario: Dashboard creation fails for non-existent dashboard group
    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"

    When I create a new dashboard for dashboard group with id 0

    Then I should get a response with response code 404 "Not found"



