Feature: Create dashboard
  Creates a new dashboard

  Scenario: Creates a new dashboard successfully
    Given dashboard's name is "dashboard1"
    And dashboard's theme is "theme"

    When I create a new dashboard

    Then I should get the dashboard given above with response code 201 "Created"

  Scenario: Dashboard creation fails due to missing mandatory parameter
    When I create a new dashboard

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name  |
      | theme |
