Feature: Update dashboard
  Updates dashboard

  Background: Dashboard group with dashboard created
    Given dashboard group's name is "dashboard group"
    And I create a new dashboard group

    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"
    And I create a new dashboard for the given dashboard group

  Scenario: Update dashboard
    Given dashboard's name is "newDashboard"
    And dashboard's theme is "newTheme"
    
    When I update given dashboard
    Then I should get a response with response code 204 "No Content"

    When I read the given dashboard group
    Then I should get the dashboard group given above with updated dashboards with response code 200 "Ok"