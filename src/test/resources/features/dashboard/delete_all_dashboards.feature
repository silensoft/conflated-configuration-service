Feature: Delete all dashboards
  deletes all dashboards

  Background: Dashboard group with dashboard created
    Given dashboard group's name is "dashboard group"
    And I create a new dashboard group

    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"
    And I create a new dashboard for the given dashboard group

  Scenario: Delete all dashboards
    When I delete all dashboards
    Then I should get a response with response code 204 "No Content"

    When I read the given dashboard group
    Then I should get the dashboard group given above without dashboards with response code 200 "Ok"