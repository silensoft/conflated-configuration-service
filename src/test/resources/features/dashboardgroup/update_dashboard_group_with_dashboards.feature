Feature: Update dashboard group with dashboards
  Updates dashboard group with dashboards

  Background:
    Given dashboard group's name is "dashboard group"
    And I create a new dashboard group

    Given dashboard's name is "dashboard"
    And dashboard's theme is "theme"
    And I create a new dashboard

    Given I add given dashboard to given dashboard group

  Scenario: Updates dashboard group with dashboard
    Given dashboard's name is "newDashboard"
    And dashboard's theme is "newTheme"
    And dashboard group's dashboards is dashboard given above

    When I update given dashboard group
    Then I should get a response with response code 204 "No Content"

    When I read the given dashboard group
    Then I should get the dashboard group given above with updated dashboards with response code 200 "Ok"