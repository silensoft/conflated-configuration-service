Feature: Update dimension
  Updates a dimension

  Scenario: Update dimension successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false
    And I create a new dimension

    When I update dimension name to "updatedFailedCallsRatio" for the above created dimension
    Then I should get a response with response code 204 "No Content"

    When I read dimension with created dimension id
    Then I should get the dimension given above with response code 200 "Ok"