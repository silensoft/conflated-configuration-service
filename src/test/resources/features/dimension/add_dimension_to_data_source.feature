Feature: Add dimension to data source
  Adds existing dimension to data source

  Background: Data source is created
    Given data source's name is "data source"
    And data source's jdbcDriverClass is "org.jdbc.driver"
    And data source's jdbcUrl is "jdbc url"
    And data source's userName is "user name"
    And data source's password is "passwd"
    And data source's sqlStatement is "SELECT FROM"
    And data source's dataRefreshIntervalValue is 15
    And data source's dataRefreshIntervalUnit is "Minutes"
    And data sources dataRefreshDelayValue is 60
    And data source's dataRefreshDelayUnit is "Seconds"
    And I create a new data source

  Scenario: Adds existing dimension to data source successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false
    And I create a new dimension

    When I add given dimension for the given data source
    Then I should get a response with response code 204 "No Content"

    When I read the given data source
    Then I should get the data source with added dimension with response code 200 "Ok"



