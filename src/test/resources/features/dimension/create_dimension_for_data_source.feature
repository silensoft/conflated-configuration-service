Feature: Create dimension for data source
  Creates a new dimension for data source

  Background: Data source is created
    Given data source's name is "data source"
    And data source's jdbcDriverClass is "org.jdbc.driver"
    And data source's jdbcUrl is "jdbc url"
    And data source's userName is "user name"
    And data source's password is "passwd"
    And data source's sqlStatement is "SELECT FROM"
    And data source's dataRefreshIntervalValue is 15
    And data source's dataRefreshIntervalUnit is "Minutes"
    And data sources dataRefreshDelayValue is 60
    And data source's dataRefreshDelayUnit is "Seconds"
    And I create a new data source

  Scenario: Creates a new dimension for data source successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false

    When I create a new dimension for the given data source
    Then I should get the dimension given above with response code 201 "Created"

    When I read the given data source
    Then I should get the data source with added dimension with response code 200 "Ok"

  Scenario: Dimension creation fails for non-existent data source
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false

    When I create a new dimension for data source with id 0

    Then I should get a response with response code 404 "Not found"



