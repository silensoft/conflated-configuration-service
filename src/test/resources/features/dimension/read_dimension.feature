Feature: Read dimension
  Reads dimension

  Scenario: Read dimensions successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false
    And I create a new dimension

    When I read dimension with created dimension id

    Then I should get the dimension given above with response code 200 "Ok"
