Feature: Delete dimension
  Deletes a dimension

  Scenario: Delete dimension successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false
    And I create a new dimension

    When I delete dimension created above
    Then I should get a response with response code 204 "No Content"

    When I read dimension with created dimension id
    Then I should get a response with response code 404 "Not Found"
