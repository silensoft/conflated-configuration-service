Feature: Create dimension
  Creates a new dimension

  Scenario: Creates a new dimension successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false

    When I create a new dimension

    Then I should get the dimension given above with response code 201 "Created"

  Scenario: Dimension creation fails due to missing mandatory parameters
    When I create a new dimension

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name        |
      | expression  |
      | isTimestamp |
      | isDate      |
      | isString    |

