Feature: Read dimensions
  Reads dimensions

  Background:
    Given All dimensions are removed from database

  Scenario: Read dimensions successfully
    Given dimension name is "failedCallsRatio"
    And dimension expression is "failedCalls / allCalls"
    And dimension isTimestamp is false
    And dimension isDate is false
    And dimension isString is false
    And I create a new dimension

    When I read dimensions

    Then I should get a list of dimensions with the dimension given above
