Feature: Create selected measure
  Creates a new selected measure

  Scenario: Creates a new selected measure successfully
    Given selected measure's name is "measure"
    And selected measure's expression is "expression"
    And selected measure's unit is ""
    And selected measures's aggregationFunction is "SUM"
    And selected measure's visualizationType is "line"
    And selected measure's visualizationColor is "#ffffff"

    When I create a selected measure

    Then I should get the selected measure given above with response code 201 "Ok"

  Scenario: Selected measure creation fails due to missing mandatory parameters
    When I create a selected measure

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name                |
      | expression          |
      | unit                |
      | aggregationFunction |
      | visualizationType   |
      | visualizationColor  |


