Feature: Create selected measure for chart
  Creates a selected measure for chart

  Background: Chart is created
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000
    And I create a new chart

  Scenario: Creates a selected measure for chart successfully
    Given selected measure's name is "measure"
    And selected measure's expression is "expression"
    And selected measure's unit is ""
    And selected measures's aggregationFunction is "SUM"
    And selected measure's visualizationType is "line"
    And selected measure's visualizationColor is "#ffffff"

    When I create a new selected measure for the given chart
    Then I should get the selected measure given above with response code 201 "Created"

    When I read the given chart
    Then I should get the chart with added selected measure with response code 200 "Ok"

  Scenario: Selected measure creation fails for non-existent chart
    Given selected measure's name is "measure"
    And selected measure's expression is "expression"
    And selected measure's unit is ""
    And selected measures's aggregationFunction is "SUM"
    And selected measure's visualizationType is "line"
    And selected measure's visualizationColor is "#ffffff"

    When I create a new selected measure for chart with id 0

    Then I should get a response with response code 404 "Not found"



