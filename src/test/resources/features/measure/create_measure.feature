Feature: Create measure
  Creates a new measure

  Scenario: Creates a new measure successfully
    Given measure name is "measure"
    And and expression is "expression"
    And unit is ""

    When I create a measure

    Then I should get the measure given above with response code 201 "Ok"

  Scenario: Measure creation fails due to missing mandatory parameters
    When I create a measure

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | name    |
      | expression |
      | unit       |

