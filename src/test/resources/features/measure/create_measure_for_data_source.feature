Feature: Create measure for data source
  Creates a new measure for data source

  Background: Data source is created
    Given data source's name is "data source"
    And data source's jdbcDriverClass is "org.jdbc.driver"
    And data source's jdbcUrl is "jdbc url"
    And data source's userName is "user name"
    And data source's password is "passwd"
    And data source's sqlStatement is "SELECT FROM"
    And data source's dataRefreshIntervalValue is 15
    And data source's dataRefreshIntervalUnit is "Minutes"
    And data sources dataRefreshDelayValue is 60
    And data source's dataRefreshDelayUnit is "Seconds"
    And I create a new data source

  Scenario: Creates a new measure for data source successfully
    Given measure name is "measure"
    And and expression is "expression"
    And unit is ""

    When I create a new measure for the given data source
    Then I should get the measure given above with response code 201 "Created"

    When I read the given data source
    Then I should get the data source with added measure with response code 200 "Ok"

  Scenario: Measure creation fails for non-existent data source
    Given measure name is "measure"
    And and expression is "expression"
    And unit is ""

    When I create a new measure for data source with id 0

    Then I should get a response with response code 404 "Not found"



