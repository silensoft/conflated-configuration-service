Feature: Create selected sort by for chart
  Creates a selected sort by for chart

  Background: Chart is created
    Given chart layout id is "1"
    And chart type is "line"
    And X-axis categories shown count is 10
    And fetchedRowCount is 1000
    And I create a new chart

  Scenario: Creates a selected sort by for chart successfully
    Given selected sort by's measureOrDimensionName is "name"
    And selected sort by's measureOrDimensionExpression is "expression"
    And selected sort by's type is "measure"
    And selected sort by's aggregationFunction is "SUM"
    And selected sort by's timeSortOption is "Latest value"
    And selected sort by's sortDirection is "ASC"
    And selected sort by's sortByDataScopeType is "all"
    And selected sort by's defaultSortByType is "none"

    When I create a new selected sort by for the given chart
    Then I should get the selected sort by given above with response code 201 "Created"

    When I read the given chart
    Then I should get the chart with added selected sort by with response code 200 "Ok"

  Scenario: Selected sort by creation fails for non-existent chart
    Given selected sort by's measureOrDimensionName is "name"
    And selected sort by's measureOrDimensionExpression is "expression"
    And selected sort by's type is "measure"
    And selected sort by's aggregationFunction is "SUM"
    And selected sort by's timeSortOption is "Latest value"
    And selected sort by's sortDirection is "ASC"
    And selected sort by's sortByDataScopeType is "all"
    And selected sort by's defaultSortByType is "none"

    When I create a new selected sort by for chart with id 0

    Then I should get a response with response code 404 "Not found"



