Feature: Create selected sort by
  Creates a new selected sort by

  Scenario: Creates a new selected sort by successfully
    Given selected sort by's measureOrDimensionName is "name"
    And selected sort by's measureOrDimensionExpression is "expression"
    And selected sort by's type is "measure"
    And selected sort by's aggregationFunction is "SUM"
    And selected sort by's timeSortOption is "Latest value"
    And selected sort by's sortDirection is "ASC"
    And selected sort by's sortByDataScopeType is "all"
    And selected sort by's defaultSortByType is "none"

    When I create a selected sort by

    Then I should get the selected sort by given above with response code 201 "Ok"

  Scenario: Selected sort by creation fails due to missing mandatory parameters
    When I create a selected sort by

    Then I should get a response with response code 400 "Bad Request"
    And response body should contain "is mandatory field" entries for following fields
      | measureOrDimensionName       |
      | measureOrDimensionExpression |
      | type                         |
      | aggregationFunction          |
      | timeSortOption               |
      | sortDirection                |
      | sortByDataScopeType          |
      | defaultSortByType            |


