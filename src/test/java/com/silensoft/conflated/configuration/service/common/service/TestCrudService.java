package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.entity.IdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public class TestCrudService extends AbstractCrudService<IdentifiableEntity> {
    private EntityStore<IdentifiableEntity> entityStore;

    @Override
    protected EntityStore<IdentifiableEntity> getEntityStore() {
        return entityStore;
    }
}
