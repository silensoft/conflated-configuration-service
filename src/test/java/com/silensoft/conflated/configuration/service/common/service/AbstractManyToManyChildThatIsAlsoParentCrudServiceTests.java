package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.Test;

import java.util.Collections;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "LawOfDemeter", "DoubleBraceInitialization"})
public class AbstractManyToManyChildThatIsAlsoParentCrudServiceTests {
    @Tested
    TestManyToManyChildThatIsAlsoParentService testService;

    @Injectable
    private EntityStore<Dashboard> entityStore;

    @Injectable
    private EntityStore<DashboardGroup> parentEntityStore;

    @Mocked
    private Dashboard updatedEntityMock;

    @Mocked
    private Dashboard currentEntityMock;

    @Mocked
    private Dashboard newEntityMock;

    @Mocked
    private DashboardGroup parentEntityMock;

    @Test
    void testUpdateEntity() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testService.getEntityStore().readEntityWithId(1L); result = currentEntityMock;
            currentEntityMock.getParentEntities(); result = Collections.singletonList(parentEntityMock);
            updatedEntityMock.hasChildEntities(); result = false;
            testService.getEntityStore().updateAndGetEntityWithId(1L, updatedEntityMock); result = newEntityMock;
            parentEntityMock.replaceChildEntity(currentEntityMock, newEntityMock);
            testService.getParentEntityStore().updateEntity(parentEntityMock);
        }};

        // WHEN
        testService.updateEntity(updatedEntityMock, 1L);

        // THEN
    }

    @Test
    void testDeleteAllEntities() {
        // GIVEN
        new Expectations() {{
           testService.getEntityStore().readEntities(); result = Collections.singletonList(currentEntityMock);
           currentEntityMock.getParentEntities(); result = Collections.singletonList(parentEntityMock);
           parentEntityMock.removeChildEntity(currentEntityMock);
           testService.getParentEntityStore().updateEntity(parentEntityMock);
           testService.getEntityStore().deleteEntity(currentEntityMock);
        }};

        // WHEN
        testService.deleteAllEntities("");
    }

    @Test
    void testDeleteEntity() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testService.getEntityStore().readEntityWithId(1L); result = currentEntityMock;
            currentEntityMock.getParentEntities(); result = Collections.singletonList(parentEntityMock);
            parentEntityMock.removeChildEntity(currentEntityMock);
            testService.getParentEntityStore().updateEntity(parentEntityMock);
            testService.getEntityStore().deleteEntity(currentEntityMock);
        }};

        // WHEN
        testService.deleteEntity(1L);
    }
}
