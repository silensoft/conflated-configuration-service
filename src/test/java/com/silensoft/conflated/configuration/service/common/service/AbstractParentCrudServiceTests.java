package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.Test;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "LawOfDemeter", "DoubleBraceInitialization"})
public class AbstractParentCrudServiceTests {
    @Tested
    private TestParentCrudService testService;

    @Injectable
    private EntityStore<Chart> entityStore;

    @Mocked
    private Chart currentEntityMock;

    @Mocked
    private Chart updatedEntityMock;

    @Test
    void testUpdateEntity() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            updatedEntityMock.hasChildEntities(); result = true;
            testService.getEntityStore().readAndDetachEntityWithId(1L); result = currentEntityMock;
            currentEntityMock.updateChildEntitiesFromOtherParentEntity(updatedEntityMock);
            testService.getEntityStore().updateEntity(currentEntityMock);
            updatedEntityMock.removeAllChildEntities();
        }};

        // WHEN
        testService.updateEntity(updatedEntityMock, 1L);

        // THEN
    }
}
