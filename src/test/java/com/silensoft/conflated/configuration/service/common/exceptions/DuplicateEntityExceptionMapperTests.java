package com.silensoft.conflated.configuration.service.common.exceptions;

import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityExceptionMapper;
import mockit.Expectations;
import mockit.Mocked;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("DoubleBraceInitialization")
public class DuplicateEntityExceptionMapperTests {

    @Mocked
    private Response responseMock;

    @Mocked
    private Response.ResponseBuilder responseBuilderMock;

    @Test
    void testToResponse() {
        // GIVEN
        //noinspection LocalVariableOfConcreteClass
        final DuplicateEntityException exception = new DuplicateEntityException("test");
        final ExceptionMapper<DuplicateEntityException> exceptionMapper = new DuplicateEntityExceptionMapper();

        new Expectations() {{
            Response.status(Response.Status.CONFLICT); result = responseBuilderMock;
            responseBuilderMock.entity(exception.getMessage()); result = responseBuilderMock;
            responseBuilderMock.build(); result = responseMock;
        }};

        // WHEN
        final Response response = exceptionMapper.toResponse(exception);

        // THEN
        assertEquals(response, responseMock);
    }
}
