package com.silensoft.conflated.configuration.service.selectedmeasure;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedMeasureServiceTests {
    @Tested
    private SelectedMeasureService selectedMeasureService;

    @Injectable
    private SelectedMeasureStore selectedMeasureStore;

    @Test
    void testGetEntityStore() {
        assertEquals(selectedMeasureService.getEntityStore(), selectedMeasureStore);
    }
}
