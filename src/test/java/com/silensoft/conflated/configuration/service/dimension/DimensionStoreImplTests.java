package com.silensoft.conflated.configuration.service.dimension;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DimensionStoreImplTests {

    @Tested
    private DimensionStoreImpl dimensionStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(dimensionStore.getEntityClass(), Dimension.class);
    }
}
