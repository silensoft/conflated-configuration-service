package com.silensoft.conflated.configuration.service.griditem;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class GridItemStoreImplTests {

    @Tested
    private GridItemStoreImpl gridItemStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(gridItemStore.getEntityClass(), GridItem.class);
    }
}
