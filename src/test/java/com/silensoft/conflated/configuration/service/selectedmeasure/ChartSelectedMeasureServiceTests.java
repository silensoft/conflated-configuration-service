package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.chart.ChartStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartSelectedMeasureServiceTests {

    @Tested
    private ChartSelectedMeasureService chartSelectedMeasureService;

    @Injectable
    private SelectedMeasureStore selectedMeasureStore;

    @Injectable
    private ChartStore chartStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(chartSelectedMeasureService.getChildEntityStore(), selectedMeasureStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(chartSelectedMeasureService.getParentEntityStore(), chartStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(chartSelectedMeasureService.getParentEntitiesName(), "chart");
    }
}
