package com.silensoft.conflated.configuration.service.dashboard;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardStoreImplTests {

    @Tested
    private DashboardStoreImpl dashboardStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(dashboardStore.getEntityClass(), Dashboard.class);
    }
}
