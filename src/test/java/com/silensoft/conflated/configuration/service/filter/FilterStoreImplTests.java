package com.silensoft.conflated.configuration.service.filter;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class FilterStoreImplTests {

    @Tested
    private FilterStoreImpl filterStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void getEntityClass() {
        assertEquals(filterStore.getEntityClass(), Filter.class);
    }
}
