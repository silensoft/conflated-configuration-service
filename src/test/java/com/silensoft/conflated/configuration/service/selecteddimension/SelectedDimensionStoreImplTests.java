package com.silensoft.conflated.configuration.service.selecteddimension;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedDimensionStoreImplTests {

    @Tested
    private SelectedDimensionStoreImpl selectedDimensionStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(selectedDimensionStore.getEntityClass(), SelectedDimension.class);
    }
}
