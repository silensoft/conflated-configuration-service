package com.silensoft.conflated.configuration.service.dashboardgroup;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardGroupStoreImplTests {

    @Tested
    private DashboardGroupStoreImpl dashboardGroupStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(dashboardGroupStore.getEntityClass(), DashboardGroup.class);
    }
}
