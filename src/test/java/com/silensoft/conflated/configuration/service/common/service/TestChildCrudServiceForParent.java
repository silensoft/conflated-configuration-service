package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;

public class TestChildCrudServiceForParent extends AbstractChildCrudServiceForParent<Dashboard, Chart> {
    private EntityStore<Chart> entityStore;

    private EntityStore<Dashboard> parentEntityStore;

    @Override
    protected EntityStore<Chart> getChildEntityStore() {
        return entityStore;
    }

    @Override
    protected EntityStore<Dashboard> getParentEntityStore() {
        return parentEntityStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dashboard";
    }
}
