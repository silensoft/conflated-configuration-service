package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DimensionTests {

    @Tested
    private Dimension dimension;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"name", "name"},
                {"expression", "expr"},
                {"isTimestamp", true},
                {"isDate", false},
                {"isString", true},
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(dimension, propertyName, propertyValue);
    }
}
