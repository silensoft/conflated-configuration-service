package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedMeasureTests {

    @Tested
    private SelectedMeasure selectedMeasure;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"name", "name"},
                {"expression",  "expr"},
                {"unit", "unit"},
                {"aggregationFunction", "SUM"},
                {"visualizationType", "line"},
                {"visualizationColor", "white"}
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(selectedMeasure, propertyName, propertyValue);
    }
}
