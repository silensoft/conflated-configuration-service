package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.dashboard.DashboardStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardChartServiceTests {
    @Tested
    private DashboardChartService dashboardChartService;

    @Injectable
    private ChartStore chartStore;

    @Injectable
    private DashboardStore dashboardStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(dashboardChartService.getChildEntityStore(), chartStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dashboardChartService.getParentEntityStore(), dashboardStore);
    }

    @Test
    void getParentEntitiesName() {
        assertEquals(dashboardChartService.getParentEntitiesName(), "dashboard");
    }
}
