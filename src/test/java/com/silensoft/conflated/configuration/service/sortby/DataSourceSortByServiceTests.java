package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceSortByServiceTests {

    @Tested
    private DataSourceSortByService dataSourceSortByService;

    @Injectable
    private SortByStore sortByStore;

    @Injectable
    private DataSourceStore dataSourceStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(dataSourceSortByService.getChildEntityStore(), sortByStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dataSourceSortByService.getParentEntityStore(), dataSourceStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(dataSourceSortByService.getParentEntitiesName(), "dataSource");
    }
}
