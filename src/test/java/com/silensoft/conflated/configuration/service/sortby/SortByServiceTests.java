package com.silensoft.conflated.configuration.service.sortby;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SortByServiceTests {

    @Tested
    private SortByService sortByService;

    @Injectable
    private SortByStore sortByStore;

    @Test
    void testGetEntityStore() {
        assertEquals(sortByService.getEntityStore(), sortByStore);
    }
}
