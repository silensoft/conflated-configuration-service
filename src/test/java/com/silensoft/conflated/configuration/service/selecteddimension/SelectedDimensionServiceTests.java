package com.silensoft.conflated.configuration.service.selecteddimension;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedDimensionServiceTests {

    @Tested
    private SelectedDimensionService selectedDimensionService;

    @Injectable
    private SelectedDimensionStore selectedDimensionStore;

    @Test
    void testGetEntityStore() {
        assertEquals(selectedDimensionService.getEntityStore(), selectedDimensionStore);
    }
}
