package com.silensoft.conflated.configuration.service.common.constants;

import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ConstantsTests {

    @SuppressWarnings("unused")
    @Tested
    private Constants constants;

    @Test
    void testConstants() {
        //noinspection MagicNumber
        assertEquals(Constants.INITIAL_CAPACITY, 20);
        assertEquals(Constants.DEFAULT_NAME_FILTER, "");
        assertEquals(Constants.ID_MUST_BE_GREATER_THAN_ZERO, "id must be greater than zero");
    }
}
