package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedSortByTests {

    @Tested
    private SelectedSortBy selectedSortBy;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"measureOrDimensionName", "name"},
                {"measureOrDimensionExpression", "expr"},
                {"type", "type"},
                {"aggregationFunction", "SUM"},
                {"timeSortOption", "latest value"},
                {"sortDirection", "ASC"},
                {"sortByDataScopeType", "all"},
                {"defaultSortByType", "sortByType"}
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(selectedSortBy, propertyName, propertyValue);
    }
}
