package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.chart.ChartStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedSortByServiceTests {

    @Tested
    private SelectedSortByService selectedSortByService;

    @Injectable
    private SelectedSortByStore selectedSortByStore;

    @Injectable
    private ChartStore chartStore;

    @Test
    void testGetEntityStore() {
        assertEquals(selectedSortByService.getEntityStore(), selectedSortByStore);
    }
}
