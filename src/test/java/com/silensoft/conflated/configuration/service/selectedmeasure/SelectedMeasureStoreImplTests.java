package com.silensoft.conflated.configuration.service.selectedmeasure;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedMeasureStoreImplTests {

    @Tested
    private SelectedMeasureStoreImpl selectedMeasureStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(selectedMeasureStore.getEntityClass(), SelectedMeasure.class);
    }
}
