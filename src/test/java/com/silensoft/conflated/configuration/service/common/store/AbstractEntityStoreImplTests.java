package com.silensoft.conflated.configuration.service.common.store;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.testng.annotations.Test;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "DoubleBraceInitialization", "LawOfDemeter", "LocalVariableOfConcreteClass"})
public class AbstractEntityStoreImplTests {
    @Tested
    private TestEntityStore testEntityStore;

    @Injectable
    private EntityManager entityManager;

    @Mocked
    private Chart entityMock;

    @Mocked
    private TypedQuery<Chart> queryMock;

    @Mocked
    private CriteriaBuilder criteriaBuilderMock;

    @Mocked
    private CriteriaQuery<Chart> criteriaQueryMock;

    @Mocked
    Root<Chart> queryRootMock;

    @Mocked
    private CriteriaDelete<Chart> criteriaDeleteMock;

    @Test
    void testCreateEntity() throws DuplicateEntityException {
        // GIVEN

        // WHEN
        testEntityStore.createEntity(entityMock);

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().persist(entityMock);
            testEntityStore.getEntityManager().flush();
        }};
    }

    @Test
    void testCreateEntity_withEntityThatAlreadyExists() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().persist(entityMock);
            result = new EntityExistsException();
        }};

        try {
            // WHEN
            testEntityStore.createEntity(entityMock);
            fail("Expected exception, but it was not thrown");
        } catch (final DuplicateEntityException exception) {
            // THEN SUCCESS
        }
    }

    @Test
    void testReadEntitiesForParent() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().createQuery("select e FROM com.silensoft.conflated.configuration.service.chart.Chart e JOIN e.dashboard p WHERE p.id = :parentId", Chart.class);
            result = queryMock;

            queryMock.setParameter("parentId", 1L);

            queryMock.getResultList();
            result = Collections.singletonList(entityMock);
        }};

        // WHEN
        final List<Chart> entities = testEntityStore.readEntitiesForParent("dashboard", 1L);

        // THEN
        assertEquals(entities.size(), 1);
        assertEquals(entities.get(0), entityMock);
    }

    @Test
    void testReadEntities() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().getCriteriaBuilder();
            result = criteriaBuilderMock;

            criteriaBuilderMock.createQuery(Chart.class);
            result = criteriaQueryMock;

            criteriaQueryMock.from(Chart.class);
            result = queryRootMock;

            criteriaQueryMock.select(queryRootMock);

            testEntityStore.getEntityManager().createQuery(criteriaQueryMock);
            result = queryMock;

            queryMock.getResultList();
            result = Collections.singletonList(entityMock);
        }};

        // WHEN
        final List<Chart> entities = testEntityStore.readEntities();

        // THEN
        assertEquals(entities.size(), 1);
        assertEquals(entities.get(0), entityMock);
    }

    @Test
    void testReadEntityWithId() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = entityMock;
        }};

        // WHEN
        final Chart entity = testEntityStore.readEntityWithId(1L);

        // THEN
        assertEquals(entity, entityMock);
    }

    @Test
    void testReadEntityWithId_whenEntityIsNotFound() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = null;
        }};

        try {
            // WHEN
            testEntityStore.readEntityWithId(1L);
            fail("EntityNotFoundException was expected to be thrown, but it was not thrown");
        } catch (final EntityNotFoundException exception) {
            // THEN SUCCESS
        }
    }

    @Test
    void testReadAndDetachEntityWithId() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = entityMock;
            testEntityStore.getEntityManager().detach(entityMock);
        }};

        // WHEN
        final Chart entity = testEntityStore.readAndDetachEntityWithId(1L);

        // THEN
        assertEquals(entity, entityMock);
    }

    @Test
    void updateEntityWithId() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = entityMock;
        }};

        // WHEN
        testEntityStore.updateEntityWithId(1L, entityMock);

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().merge(entityMock);
        }};
    }

    @Test
    void updateEntityWithId_whenEntityIsNotFound() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = null;
        }};

        try {
            // WHEN
            testEntityStore.updateEntityWithId(1L, entityMock);
            fail("EntityNotFoundException was expected to be thrown, but it was not thrown");
        } catch (final EntityNotFoundException exception) {
            // THEN SUCCESS
        }
    }

    @Test
    void testUpdateAndGetEntityWithId() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = entityMock;

            testEntityStore.getEntityManager().merge(entityMock);
            result = entityMock;
        }};

        // WHEN
        final Chart entity = testEntityStore.updateAndGetEntityWithId(1L, entityMock);

        // THEN
        assertEquals(entity, entityMock);
    }

    @Test
    void testUpdateAndGetEntityWithId_whenEntityIsNotFound() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = null;
        }};

        try {
            // WHEN
            testEntityStore.updateAndGetEntityWithId(1L, entityMock);
            fail("EntityNotFoundException was expected to be thrown, but was not thrown");
        } catch (final EntityNotFoundException exception) {
            // THEN SUCCESS
        }
    }

    @Test
    void testUpdateEntity() {
        // GIVEN

        // WHEN
        testEntityStore.updateEntity(entityMock);

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().merge(entityMock);
            testEntityStore.getEntityManager().flush();
        }};
    }

    @Test
    void testDeleteAllEntities() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().getCriteriaBuilder();
            result = criteriaBuilderMock;

            criteriaBuilderMock.createCriteriaDelete(Chart.class);
            result = criteriaDeleteMock;
        }};

        // WHEN
        testEntityStore.deleteAllEntities();

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().createQuery(criteriaDeleteMock).executeUpdate();
        }};
    }

    @Test
    void testDeleteEntityWithId() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = entityMock;
        }};

        // WHEN
        testEntityStore.deleteEntityWithId(1L);

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().remove(entityMock);
        }};
    }

    @Test
    void testDeleteEntityWithId_whenEntityIsNotFound() {
        // GIVEN
        new Expectations() {{
            testEntityStore.getEntityManager().find(Chart.class, 1L);
            result = null;
        }};

        try {
            // WHEN
            testEntityStore.deleteEntityWithId(1L);
            fail("EntityNotFoundException was expected to be thrown, but was not thrown");
        } catch (final EntityNotFoundException exception) {
            // THEN
            // SUCCESS
        }
    }

    @Test
    void testDeleteEntity() {
        // GIVEN

        // WHEN
        testEntityStore.deleteEntity(entityMock);

        // THEN
        new Verifications() {{
            testEntityStore.getEntityManager().remove(entityMock);
        }};
    }
}
