package com.silensoft.conflated.configuration.service.datasource;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceServiceTests {

    @Tested
    private DataSourceService dataSourceService;

    @Injectable
    private DataSourceStore dataSourceStore;

    @Test
    void testGetEntityStore() {
        assertEquals(dataSourceService.getEntityStore(), dataSourceStore);
    }
}
