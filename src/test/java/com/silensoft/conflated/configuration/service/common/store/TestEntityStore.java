package com.silensoft.conflated.configuration.service.common.store;

import com.silensoft.conflated.configuration.service.chart.Chart;

public class TestEntityStore extends AbstractEntityStoreImpl<Chart> {
    @Override
    protected Class<Chart> getEntityClass() {
        return Chart.class;
    }
}
