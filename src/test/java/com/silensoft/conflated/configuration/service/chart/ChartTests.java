package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.TestUtils;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.selecteddimension.SelectedDimension;
import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilter;
import com.silensoft.conflated.configuration.service.selectedmeasure.SelectedMeasure;
import com.silensoft.conflated.configuration.service.selectedsortby.SelectedSortBy;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartTests {

    @Tested
    private Chart chart;

    @Tested
    private Chart otherChart;

    @Mocked
    private SelectedMeasure selectedMeasure;

    @Mocked
    private SelectedDimension selectedDimension;

    @Mocked
    private SelectedFilter selectedFilter;

    @Mocked
    private SelectedSortBy selectedSortBy;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"layoutId", "1"},
                {"dataSource", new DataSource()},
                {"chartType", "line"},
                {"xAxisCategoriesShownCount", 10},
                {"fetchedRowCount", 1000},
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(chart, propertyName, propertyValue);
    }

    @Test
    void testChildEntityOperations() {
        TestUtils.testChildEntityOperations(chart, Arrays.asList(selectedMeasure, selectedDimension, selectedFilter, selectedSortBy), otherChart);
    }
}
