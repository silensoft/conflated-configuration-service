package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.Test;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class AbstractDataSourceChildEntityTests {
    @Tested
    private AbstractDataSourceChildEntity abstractDataSourceChildEntity;

    @Test
    void testSetAndGetParentEntity() {
        TestUtils.testSetAndGetParentEntity(abstractDataSourceChildEntity, new DataSource());
    }
}
