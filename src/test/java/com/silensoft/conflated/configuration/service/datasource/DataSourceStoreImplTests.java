package com.silensoft.conflated.configuration.service.datasource;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceStoreImplTests {

    @Tested
    private DataSourceStoreImpl dataSourceStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(dataSourceStore.getEntityClass(), DataSource.class);
    }
}
