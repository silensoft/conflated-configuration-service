package com.silensoft.conflated.configuration.service.measure;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class MeasureServiceTests {

    @Tested
    private MeasureService measureService;

    @Injectable
    private MeasureStore measureStore;

    @Test
    void testGetEntityStore() {
        assertEquals(measureService.getEntityStore(), measureStore);
    }
}
