package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.dashboard.DashboardStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartServiceTests {
    @Tested
    private ChartService chartService;

    @Injectable
    private ChartStore chartStore;

    @Injectable
    private DashboardStore dashboardStore;

    @Test
    void testGetEntityStore() {
        assertEquals(chartService.getEntityStore(), chartStore);
    }
}
