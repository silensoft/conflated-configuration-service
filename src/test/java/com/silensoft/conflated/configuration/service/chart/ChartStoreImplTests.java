package com.silensoft.conflated.configuration.service.chart;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartStoreImplTests {
    @Tested
    private ChartStoreImpl chartStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityManager() {
        assertEquals(chartStore.getEntityManager(), entityManager);
    }

    @Test
    void testGetEntityClass() {
        assertEquals(chartStore.getEntityClass(), Chart.class);
    }
}
