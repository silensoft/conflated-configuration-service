package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.TestUtils;
import com.silensoft.conflated.configuration.service.dimension.Dimension;
import com.silensoft.conflated.configuration.service.filter.Filter;
import com.silensoft.conflated.configuration.service.measure.Measure;
import com.silensoft.conflated.configuration.service.sortby.SortBy;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceTests {
    @Tested
    private DataSource dataSource;

    @Tested
    private DataSource otherDataSource;

    @Mocked
    private Measure measureMock;

    @Mocked
    private Dimension dimensionMock;

    @Mocked
    private Filter filterMock;

    @Mocked
    private SortBy sortByMock;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"name", "name"},
                {"jdbcDriverClass", "jdbc"},
                {"jdbcUrl", "jdbc"},
                {"userName", "John"},
                {"password", "passwd"},
                {"sqlStatement", "select"},
                {"dataRefreshIntervalValue", 1L},
                {"dataRefreshIntervalUnit", "min"},
                {"dataRefreshDelayValue", 0L},
                {"dataRefreshDelayUnit", "sec"}
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(dataSource, propertyName, propertyValue);
    }

    @Test
    void testChildEntityOperations() {
        TestUtils.testChildEntityOperations(dataSource, Arrays.asList(measureMock, dimensionMock, filterMock, sortByMock), otherDataSource);
    }
}
