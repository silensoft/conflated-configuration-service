package com.silensoft.conflated.configuration.service.filter;

import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceFilterServiceTests {

    @Tested
    private DataSourceFilterService dataSourceFilterService;

    @Injectable
    private FilterStore filterStore;

    @Injectable
    private DataSourceStore dataSourceStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(dataSourceFilterService.getChildEntityStore(), filterStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dataSourceFilterService.getParentEntityStore(), dataSourceStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(dataSourceFilterService.getParentEntitiesName(), "dataSource");
    }
}
