package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedDimensionTests {

    @Tested
    private SelectedDimension selectedDimension;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"name", "name"},
                {"expression", "expr"},
                {"isTimestamp", true},
                {"isDate", false},
                {"isString", true},
                {"visualizationType", "legend"}
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(selectedDimension, propertyName, propertyValue);
    }
}
