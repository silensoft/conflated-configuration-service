package com.silensoft.conflated.configuration.service.chart;
import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.Test;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class AbstractChartChildEntityTests {

    @Tested
    private AbstractChartChildEntity abstractChartChildEntity;

    @Test
    void testSetAndGetParentEntity() {
        TestUtils.testSetAndGetParentEntity(abstractChartChildEntity, new Chart());
    }
}
