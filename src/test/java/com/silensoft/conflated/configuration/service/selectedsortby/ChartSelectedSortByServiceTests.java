package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.chart.ChartStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartSelectedSortByServiceTests {

    @Tested
    private ChartSelectedSortByService chartSelectedSortByService;

    @Injectable
    private SelectedSortByStore selectedSortByStore;

    @Injectable
    private ChartStore chartStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(chartSelectedSortByService.getChildEntityStore(), selectedSortByStore);
    }

    @Test
    void testParentEntityStore() {
        assertEquals(chartSelectedSortByService.getParentEntityStore(), chartStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(chartSelectedSortByService.getParentEntitiesName(), "chart");
    }
}
