package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public class TestParentCrudService extends AbstractParentCrudService<Chart> {
    private EntityStore<Chart> entityStore;

    @Override
    protected EntityStore<Chart> getEntityStore() {
        return entityStore;
    }
}

