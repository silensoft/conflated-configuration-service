package com.silensoft.conflated.configuration.service.measure;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class MeasureStoreImplTests {

    @Tested
    private MeasureStoreImpl measureStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(measureStore.getEntityClass(), Measure.class);
    }
}
