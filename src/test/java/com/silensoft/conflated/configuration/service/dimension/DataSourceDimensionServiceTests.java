package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceDimensionServiceTests {

    @Tested
    private DataSourceDimensionService dataSourceDimensionService;

    @Injectable
    private DimensionStore dimensionStore;

    @Injectable
    private DataSourceStore dataSourceStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(dataSourceDimensionService.getChildEntityStore(), dimensionStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dataSourceDimensionService.getParentEntityStore(), dataSourceStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(dataSourceDimensionService.getParentEntitiesName(), "dataSource");
    }
}
