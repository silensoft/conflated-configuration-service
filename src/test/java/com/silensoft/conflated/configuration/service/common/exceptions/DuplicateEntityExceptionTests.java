package com.silensoft.conflated.configuration.service.common.exceptions;

import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DuplicateEntityExceptionTests {

    @Test
    void testCreationOfDuplicateEntityException() {
        // GIVEN

        // WHEN
        final Exception exception = new DuplicateEntityException("text");

        // THEN
        assertEquals(exception.getMessage(), "text");
    }
}
