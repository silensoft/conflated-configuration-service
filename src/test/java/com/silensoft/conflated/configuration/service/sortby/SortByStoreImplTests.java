package com.silensoft.conflated.configuration.service.sortby;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SortByStoreImplTests {
    @Tested
    private SortByStoreImpl sortByStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(sortByStore.getEntityClass(), SortBy.class);
    }
}
