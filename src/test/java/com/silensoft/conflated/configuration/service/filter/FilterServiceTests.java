package com.silensoft.conflated.configuration.service.filter;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class FilterServiceTests {

    @Tested
    private FilterService filterService;

    @Injectable
    private FilterStore filterStore;

    @Test
    void testGetEntityStore() {
        assertEquals(filterService.getEntityStore(), filterStore);
    }
}
