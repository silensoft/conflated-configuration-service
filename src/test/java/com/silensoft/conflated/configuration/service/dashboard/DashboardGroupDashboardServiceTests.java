package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardGroupDashboardServiceTests {

    @Tested
    private DashboardGroupDashboardService dashboardGroupDashboardService;

    @Injectable
    private DashboardStore dashboardStore;

    @Injectable
    private DashboardGroupStore dashboardGroupStore;

    @Test
    void testGetParentEntityStore() {
        assertEquals(dashboardGroupDashboardService.getParentEntityStore(), dashboardGroupStore);
    }

    @Test
    void testGetChildEntityStore() {
        assertEquals(dashboardGroupDashboardService.getChildEntityStore(), dashboardStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(dashboardGroupDashboardService.getParentEntitiesName(), "dashboardGroups");
    }
}
