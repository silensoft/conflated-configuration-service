package com.silensoft.conflated.configuration.service.griditem;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class GridItemServiceTests {

    @Tested
    private GridItemService gridItemService;

    @Injectable
    private GridItemStore gridItemStore;

    @Test
    void testGetEntityStore() {
        assertEquals(gridItemService.getEntityStore(), gridItemStore);
    }
}
