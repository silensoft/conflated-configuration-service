package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardServiceTests {

    @Tested
    private DashboardService dashboardService;

    @Injectable
    private DashboardStore dashboardStore;

    @Injectable
    private DashboardGroupStore dashboardGroupStore;

    @Test
    void testGetEntityStore() {
        assertEquals(dashboardService.getEntityStore(), dashboardStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dashboardService.getParentEntityStore(), dashboardGroupStore);
    }
}
