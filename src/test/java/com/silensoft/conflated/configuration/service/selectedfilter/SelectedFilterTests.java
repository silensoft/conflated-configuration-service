package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.TestUtils;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedFilterTests {

    @Tested
    private SelectedFilter selectedFilter;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{
                {"measureOrDimensionName", "name"},
                {"measureOrDimensionExpression", "expr"},
                {"type", "type"},
                {"aggregationFunction", "SUM"},
                {"filterExpression", "filterExpr"},
                {"filterInputType", "radio"},
                {"filterInputType", "radio"},
                {"filterDataScopeType", "all"},
                {"allowedDimensionFilterInputTypes", "radio,checkbox"}
        };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(selectedFilter, propertyName, propertyValue);
    }
}
