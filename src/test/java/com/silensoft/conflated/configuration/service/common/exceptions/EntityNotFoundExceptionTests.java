package com.silensoft.conflated.configuration.service.common.exceptions;

import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class EntityNotFoundExceptionTests {

    @Test
    void testCreationOfEntityNotFoundException() {
        // GIVEN

        // WHEN
        final Exception exception = new EntityNotFoundException("text");

        // THEN
        assertEquals(exception.getMessage(), "text");
    }
}
