package com.silensoft.conflated.configuration.service.dimension;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DimensionServiceTests {

    @Tested
    private DimensionService dimensionService;

    @Injectable
    private DimensionStore dimensionStore;

    @Test
    void testGetEntityStore() {
        assertEquals(dimensionService.getEntityStore(), dimensionStore);
    }
}
