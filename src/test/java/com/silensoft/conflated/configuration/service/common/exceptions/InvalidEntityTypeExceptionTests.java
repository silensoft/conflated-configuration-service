package com.silensoft.conflated.configuration.service.common.exceptions;

import com.silensoft.conflated.configuration.service.common.exceptions.entity.InvalidEntityTypeException;
import org.testng.annotations.Test;

public class InvalidEntityTypeExceptionTests {

    @Test
    void testCreationOfInvalidEntityTypeException() {
        // GIVEN

        // WHEN
        //noinspection ThrowableNotThrown
        new InvalidEntityTypeException();

        // THEN
    }
}
