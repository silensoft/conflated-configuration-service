package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.entity.IdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "DoubleBraceInitialization", "LawOfDemeter"})
public class AbstractCrudServiceTests {

    @Tested
    private TestCrudService testService;

    @Injectable
    private EntityStore<IdentifiableEntity> entityStore;

    @Mocked
    private IdentifiableEntity entityMock;

    @Mocked
    UriInfo uriInfoMock;

    @Mocked
    UriBuilder uriBuilderMock;

    @Mocked
    private Response responseMock;

    @Mocked
    private URI uriMock;

    @Mocked
    private Response.ResponseBuilder responseBuilderMock;

    @Test
    void testCreateEntity() throws DuplicateEntityException {
        // GIVEN
        new Expectations() {{
            testService.getEntityStore().createEntity(entityMock);
            uriInfoMock.getAbsolutePathBuilder(); result = uriBuilderMock;
            uriBuilderMock.path("{id}"); result = uriBuilderMock;
            entityMock.getId(); result = 1L;
            uriBuilderMock.build(1L); result = uriMock ;
            Response.created(uriMock); result = responseBuilderMock;
            responseBuilderMock.entity(entityMock); result = responseBuilderMock;
            responseBuilderMock.build(); result = responseMock;
        }};

        // WHEN
        final Response response = testService.createEntity(entityMock, uriInfoMock);

        // THEN
        assertEquals(response, responseMock);
    }

    @Test
    void testReadEntities() {
        // GIVEN
        new Expectations() {{
            testService.getEntityStore().readEntities(); result = Collections.singletonList(entityMock);
        }};

        // WHEN
        final List<IdentifiableEntity> entities = testService.readEntities("");

        // THEN
        assertEquals(entities.size(), 1);
        assertEquals(entities.get(0), entityMock);
    }

    @Test
    void testReadEntity() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testService.getEntityStore().readEntityWithId(1L); result = entityMock;
        }};

        // WHEN
        final IdentifiableEntity entity = testService.readEntity(1L);

        // THEN
        assertEquals(entity, entityMock);
    }

    @Test
    void testUpdateEntity() throws EntityNotFoundException {
        // WHEN
        testService.updateEntity(entityMock, 1L);

        // THEN
        new Verifications() {{
            testService.getEntityStore().updateEntityWithId(1L, entityMock);
        }};
    }

    @Test
    void testDeleteAllEntities() {
        // WHEN
        testService.deleteAllEntities("");

        // THEN
        new Verifications() {{
           testService.getEntityStore().deleteAllEntities();
        }};
    }

    @Test
    void testDeleteEntity() throws EntityNotFoundException {
        // WHEN
        testService.deleteEntity(1L);

        // THEN
        new Verifications() {{
            testService.getEntityStore().deleteEntityWithId(1L);
        }};
    }
}
