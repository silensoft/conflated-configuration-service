package com.silensoft.conflated.configuration.service.selectedfilter;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedFilterServiceTests {

    @Tested
    private SelectedFilterService selectedFilterService;

    @Injectable
    private SelectedFilterStore selectedFilterStore;

    @Test
    void testGetEntityStore() {
        assertEquals(selectedFilterService.getEntityStore(), selectedFilterStore);
    }
}
