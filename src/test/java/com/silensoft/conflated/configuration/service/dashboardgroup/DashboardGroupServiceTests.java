package com.silensoft.conflated.configuration.service.dashboardgroup;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardGroupServiceTests {

    @Tested
    private DashboardGroupService dashboardGroupService;

    @Injectable
    private DashboardGroupStore dashboardGroupStore;

    @Test
    void testGetEntityStore() {
        assertEquals(dashboardGroupService.getEntityStore(), dashboardGroupStore);
    }
}
