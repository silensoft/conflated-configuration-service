package com.silensoft.conflated.configuration.service.common.entity;

import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class AbstractIdentifiableEntityTests {
    @Tested
    private AbstractIdentifiableEntity abstractIdentifiableEntity;

    @Test
    void testSetAndGetId() {
        // GIVEN
        abstractIdentifiableEntity.setId(1L);

        // WHEN
        final long id = abstractIdentifiableEntity.getId();

        // THEN
        assertEquals(id, 1L);
    }
}
