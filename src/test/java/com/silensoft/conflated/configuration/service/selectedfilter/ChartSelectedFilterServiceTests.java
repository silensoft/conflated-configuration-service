package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.chart.ChartStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartSelectedFilterServiceTests {

    @Tested
    private ChartSelectedFilterService chartSelectedFilterService;

    @Injectable
    private SelectedFilterStore selectedDimensionStore;

    @Injectable
    private ChartStore chartStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(chartSelectedFilterService.getChildEntityStore(), selectedDimensionStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(chartSelectedFilterService.getParentEntityStore(), chartStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(chartSelectedFilterService.getParentEntitiesName(), "chart");
    }
}
