package com.silensoft.conflated.configuration.service.common.types;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings({"LocalVariableOfConcreteClass", "LawOfDemeter"})
public class OptionalTests {
    @Test
    void testOf() throws Exception {
        // GIVEN

        // WHEN
        final Optional<String> possibleString = Optional.of("test");
        final String str = possibleString.orElseThrow(new Exception());

        // THEN
        assertEquals(str, "test");
    }
}
