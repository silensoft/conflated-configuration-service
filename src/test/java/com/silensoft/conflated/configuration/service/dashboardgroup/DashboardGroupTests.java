package com.silensoft.conflated.configuration.service.dashboardgroup;

import com.silensoft.conflated.configuration.service.TestUtils;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import mockit.Mock;
import mockit.MockUp;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardGroupTests {

    @Tested
    private DashboardGroup dashboardGroup;

    @Tested
    private DashboardGroup otherDashboardGroup;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{{"name", "name"}};
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(dashboardGroup, propertyName, propertyValue);
    }

    @Test
    void testGetChildEntityOperations() {
        new MockUp<Dashboard>() {
            @Mock
            List<DashboardGroup> getParentEntities() {
                return new ArrayList<>(1);
            }
        };

        TestUtils.testChildEntityOperations(dashboardGroup, Collections.singletonList(new Dashboard()), otherDashboardGroup);
    }

}
