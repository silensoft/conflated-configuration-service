package com.silensoft.conflated.configuration.service.selectedsortby;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedSortByStoreImplTests {

    @Tested
    private SelectedSortByStoreImpl selectedSortByStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(selectedSortByStore.getEntityClass(), SelectedSortBy.class);
    }
}
