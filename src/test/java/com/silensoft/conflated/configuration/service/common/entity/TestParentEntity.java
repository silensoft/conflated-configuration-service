package com.silensoft.conflated.configuration.service.common.entity;

import java.util.ArrayList;
import java.util.List;

public class TestParentEntity implements ParentEntity<String> {
    private final List<ChildEntityOf<String>> childEntities = new ArrayList<>(1);

    @Override
    public void addChildEntity(final ChildEntityOf<String> childEntity) {
        childEntities.add(childEntity);
    }

    @Override
    public void removeChildEntity(final ChildEntityOf<String> childEntity) {
        childEntities.remove(childEntity);
    }

    @Override
    public List<ChildEntityOf<String>> getChildEntities(final ChildEntityOf<String> childEntity) {
        return new ArrayList<>(childEntities);
    }

    @Override
    public void updateChildEntitiesFromOtherParentEntity(final ParentEntity<String> otherParentEntity) {
    }

    @Override
    public void removeAllChildEntities(final ChildEntityOf<String> childEntity) {
        childEntities.clear();
    }

    @Override
    public void removeAllChildEntities() {
        childEntities.clear();
    }

    @Override
    public boolean hasChildEntities() {
        return false;
    }

    @Override
    public long getId() {
        return 0L;
    }

    @Override
    public void setId(final long newId) {
    }
}
