package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.dashboard.DashboardStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardGridItemServiceTests {

    @Tested
    private DashboardGridItemService dashboardGridItemService;

    @Injectable
    private GridItemStore gridItemStore;

    @Injectable
    private DashboardStore dashboardStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(dashboardGridItemService.getChildEntityStore(), gridItemStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(dashboardGridItemService.getParentEntityStore(), dashboardStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(dashboardGridItemService.getParentEntitiesName(), "dashboard");
    }
}
