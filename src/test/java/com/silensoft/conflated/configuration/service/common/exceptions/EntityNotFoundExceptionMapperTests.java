package com.silensoft.conflated.configuration.service.common.exceptions;

import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundExceptionMapper;
import mockit.Expectations;
import mockit.Mocked;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static org.testng.Assert.assertEquals;

public class EntityNotFoundExceptionMapperTests {
    @Mocked
    private Response responseMock;

    @Mocked
    private Response.ResponseBuilder responseBuilderMock;

    @Test
    void testToResponse() {
        // GIVEN
        //noinspection LocalVariableOfConcreteClass
        final EntityNotFoundException exception = new EntityNotFoundException("test");
        final ExceptionMapper<EntityNotFoundException> exceptionMapper = new EntityNotFoundExceptionMapper();

        //noinspection DoubleBraceInitialization
        new Expectations() {{
            Response.status(Response.Status.NOT_FOUND); result = responseBuilderMock;
            responseBuilderMock.entity(exception.getMessage()); result = responseBuilderMock;
            responseBuilderMock.build(); result = responseMock;
        }};

        // WHEN
        final Response response = exceptionMapper.toResponse(exception);

        // THEN
        assertEquals(response, responseMock);
    }
}
