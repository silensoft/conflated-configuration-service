package com.silensoft.conflated.configuration.service;

import com.silensoft.conflated.configuration.service.ConflatedConfigurationServiceApplication;
import com.silensoft.conflated.configuration.service.chart.ChartService;
import com.silensoft.conflated.configuration.service.chart.DashboardChartService;
import com.silensoft.conflated.configuration.service.common.exceptions.ConstraintViolationExceptionMapper;
import com.silensoft.conflated.configuration.service.dashboard.DashboardGroupDashboardService;
import com.silensoft.conflated.configuration.service.dashboard.DashboardService;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupService;
import com.silensoft.conflated.configuration.service.datasource.DataSourceService;
import com.silensoft.conflated.configuration.service.dimension.DataSourceDimensionService;
import com.silensoft.conflated.configuration.service.dimension.DimensionService;
import com.silensoft.conflated.configuration.service.filter.DataSourceFilterService;
import com.silensoft.conflated.configuration.service.filter.FilterService;
import com.silensoft.conflated.configuration.service.griditem.DashboardGridItemService;
import com.silensoft.conflated.configuration.service.griditem.GridItemService;
import com.silensoft.conflated.configuration.service.measure.DataSourceMeasureService;
import com.silensoft.conflated.configuration.service.measure.MeasureService;
import com.silensoft.conflated.configuration.service.selecteddimension.ChartSelectedDimensionService;
import com.silensoft.conflated.configuration.service.selecteddimension.SelectedDimensionService;
import com.silensoft.conflated.configuration.service.selectedfilter.ChartSelectedFilterService;
import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilterService;
import com.silensoft.conflated.configuration.service.selectedmeasure.ChartSelectedMeasureService;
import com.silensoft.conflated.configuration.service.selectedmeasure.SelectedMeasureService;
import com.silensoft.conflated.configuration.service.selectedsortby.ChartSelectedSortByService;
import com.silensoft.conflated.configuration.service.selectedsortby.SelectedSortByService;
import com.silensoft.conflated.configuration.service.sortby.DataSourceSortByService;
import com.silensoft.conflated.configuration.service.sortby.SortByService;
import org.testng.annotations.Test;

import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.Set;

import static org.testng.AssertJUnit.assertTrue;

public class ConflatedConfigurationServiceApplicationTests {

    @Test
    void testGetClasses() {
        // GIVEN
        final Application application = new ConflatedConfigurationServiceApplication();

        // WHEN
        final Set<Class<?>> classes = application.getClasses();

        // THEN
        assertTrue(classes.containsAll(Arrays.asList(
                ChartService.class,
                DashboardChartService.class,
                DataSourceService.class,
                DataSourceMeasureService.class,
                MeasureService.class,
                DataSourceDimensionService.class,
                DimensionService.class,
                DataSourceFilterService.class,
                FilterService.class,
                DataSourceSortByService.class,
                SortByService.class,
                DashboardGroupService.class,
                DashboardGroupDashboardService.class,
                DashboardService.class,
                DashboardGridItemService.class,
                GridItemService.class,
                ChartSelectedDimensionService.class,
                SelectedDimensionService.class,
                ChartSelectedFilterService.class,
                SelectedFilterService.class,
                ChartSelectedMeasureService.class,
                SelectedMeasureService.class,
                ChartSelectedSortByService.class,
                SelectedSortByService.class,
                ConstraintViolationExceptionMapper.class
        )));
    }
}
