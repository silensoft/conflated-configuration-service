package com.silensoft.conflated.configuration.service.common.exceptions;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;

import java.util.Collections;

import static org.testng.Assert.assertEquals;

@SuppressWarnings({"DoubleBraceInitialization", "InstanceVariableOfConcreteClass"})
public class ConstraintViolationExceptionMapperTests {

    @Tested
    private ConstraintViolationExceptionMapper constraintViolationExceptionMapper;

    @Mocked
    private Response responseMock;

    @Mocked
    private Response.ResponseBuilder responseBuilderMock;

    @Test
    void testToResponse() {
        // GIVEN
        final ConstraintViolationException constraintViolationException =
                new ConstraintViolationException("test", Collections.singleton(new TestConstraintViolation()));

        new Expectations() {{
            Response.status(Response.Status.BAD_REQUEST); result = responseBuilderMock;
            responseBuilderMock.entity(Collections.singletonList("Test constraint violation")); result = responseBuilderMock;
            responseBuilderMock.build(); result = responseMock;
        }};

        // WHEN
        final Response response = constraintViolationExceptionMapper.toResponse(constraintViolationException);

        // THEN
        assertEquals(response, responseMock);
    }
}
