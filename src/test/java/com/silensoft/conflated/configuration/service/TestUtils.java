package com.silensoft.conflated.configuration.service;

import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import mockit.Verifications;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@SuppressWarnings("DoubleBraceInitialization")
public final class TestUtils {
    private TestUtils() {
    }

    public static <P extends ParentEntity<P>> void testAddChildEntity(final P parentEntity, final ChildEntityOf<P> childEntity) {
        // WHEN
        parentEntity.addChildEntity(childEntity);

        // THEN
        new Verifications() {{
            childEntity.setParentEntity(parentEntity);
            final List<ChildEntityOf<P>> childEntities = parentEntity.getChildEntities(childEntity);
            assertEquals(childEntities.size(), 1);
            assertEquals(childEntities.get(0), childEntity);
        }};

        // CLEANUP
        parentEntity.removeChildEntity(childEntity);
    }

    public static <P> void testRemoveChildEntity(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);

        // WHEN
        parentEntity.removeChildEntity(childEntity);

        // THEN
        new Verifications() {{
            childEntity.setParentEntity(null);
            final List<ChildEntityOf<P>> childEntities = parentEntity.getChildEntities(childEntity);
            assertEquals(childEntities.size(), 0);
        }};
    }

    public static <P> void testGetChildEntities(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);

        // WHEN
        final List<ChildEntityOf<P>> childEntities = parentEntity.getChildEntities(childEntity);

        // THEN
        assertEquals(childEntities.size(), 1);
        assertEquals(childEntities.get(0), childEntity);
    }

    public static <P> void testRemoveAllChildEntitiesOfType(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);

        // WHEN
        parentEntity.removeAllChildEntities(childEntity);

        // THEN
        assertEquals(parentEntity.getChildEntities(childEntity).size(), 0);
    }

    public static <P> void testRemoveAllChildEntities(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);

        // WHEN
        parentEntity.removeAllChildEntities();

        // THEN
        assertEquals(parentEntity.getChildEntities(childEntity).size(), 0);
    }

    public static <P> void testHasChildEntities(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);

        // WHEN
        final boolean hasChildEntities = parentEntity.hasChildEntities();

        // THEN
        assertTrue(hasChildEntities);

        // CLEANUP
        parentEntity.removeChildEntity(childEntity);
    }

    public static <P> void testUpdateChildEntitiesFromOtherParentEntity(final ParentEntity<P> parentEntity, final ChildEntityOf<P> childEntity, final ParentEntity<P> otherParentEntity) {
        // GIVEN
        parentEntity.addChildEntity(childEntity);
        otherParentEntity.addChildEntity(childEntity);

        // WHEN
        parentEntity.updateChildEntitiesFromOtherParentEntity(otherParentEntity);

        // THEN
        final List<ChildEntityOf<P>> childEntities = parentEntity.getChildEntities(childEntity);
        assertEquals(childEntities.size(), 1);
        assertEquals(childEntities.get(0), childEntity);
    }

    public static <P extends ParentEntity<P>> void testChildEntityOperations(final P parentEntity, final Iterable<ChildEntityOf<P>> childEntities, final P otherParentEntity) {
        for (final ChildEntityOf<P> childEntity : childEntities) {
            testAddChildEntity(parentEntity, childEntity);
            testRemoveChildEntity(parentEntity, childEntity);
            testGetChildEntities(parentEntity, childEntity);
            testRemoveAllChildEntitiesOfType(parentEntity, childEntity);
            testRemoveAllChildEntities(parentEntity, childEntity);
            testHasChildEntities(parentEntity, childEntity);
            testUpdateChildEntitiesFromOtherParentEntity(parentEntity, childEntity, otherParentEntity);
        }
    }

    public static <P extends ParentEntity<P>> void testSetAndGetParentEntity(final ChildEntityOf<P> childEntity, final P parentEntity) {
        // GIVEN
        childEntity.setParentEntity(parentEntity);

        // WHEN
        final List<P> parentEntities = childEntity.getParentEntities();

        // THEN
        assertEquals(parentEntities.size(), 1);
        assertEquals(parentEntities.get(0), parentEntity);
    }

    public static <E, T> void testSetAndGetProperty(final E entity, final String propertyName, final T propertyValue) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final boolean hasUpperCaseCharAtBeginning = propertyName.length() >= 2 && (Character.isUpperCase(propertyName.charAt(0)) || Character.isUpperCase(propertyName.charAt(1)));
        final String accessorPostFix = hasUpperCaseCharAtBeginning ? propertyName : propertyName.substring(0, 1).toUpperCase(Locale.ENGLISH) + propertyName.substring(1);
        final Method setterMethod = entity.getClass().getMethod("set" + accessorPostFix, propertyValue.getClass());
        final Method getterMethod = entity.getClass().getMethod("get" + accessorPostFix);

        // GIVEN
        setterMethod.invoke(entity, propertyValue);

        // WHEN
        final Object value = getterMethod.invoke(entity);

        // THEN
        assertEquals(value, propertyValue);
    }
}
