package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;

public class TestManyToManyChildThatIsAlsoParentService extends AbstractManyToManyChildThatIsAlsoParentCrudService<DashboardGroup, Dashboard> {
    private EntityStore<Dashboard> entityStore;

    private EntityStore<DashboardGroup> parentEntityStore;

    @Override
    protected EntityStore<Dashboard> getEntityStore() {
        return entityStore;
    }

    @Override
    protected EntityStore<DashboardGroup> getParentEntityStore() {
        return parentEntityStore;
    }
}
