package com.silensoft.conflated.configuration.service.dashboard;

import mockit.Tested;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "LocalVariableOfConcreteClass"})
public class AbstractDashboardChildEntityTests {

    @Tested
    private AbstractDashboardChildEntity abstractDashboardChildEntity;

    @Test
    void testSetAndGetParentEntity() {
        // GIVEN
        final Dashboard dashboard = new Dashboard();
        abstractDashboardChildEntity.setParentEntity(dashboard);

        // WHEN
        final List<Dashboard> parentEntities = abstractDashboardChildEntity.getParentEntities();

        // THEN
        assertEquals(parentEntities.size(), 1);
        assertEquals(parentEntities.get(0), dashboard);
    }
}
