package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.TestUtils;
import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import com.silensoft.conflated.configuration.service.griditem.GridItem;
import mockit.Mocked;
import mockit.Tested;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DashboardTests {

    @Tested
    private Dashboard dashboard;

    @Tested
    private Dashboard otherDashboard;

    @Mocked
    private Chart chartMock;

    @Mocked
    private GridItem gridItemMock;

    @DataProvider
    public Object[][] getPropertyNamesAndValues() {
        return new Object[][]{{"name", "name"}, {"theme", "theme"} };
    }

    @Test(dataProvider = "getPropertyNamesAndValues")
    void testSetAndGetProperties(final String propertyName, final Object propertyValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        TestUtils.testSetAndGetProperty(dashboard, propertyName, propertyValue);
    }

    @Test
    void testChildEntityOperations() {
        TestUtils.testChildEntityOperations(dashboard, Arrays.asList(chartMock, gridItemMock), otherDashboard);
    }

    @Test
    void testSetAndGetParentEntity() {
        TestUtils.testSetAndGetParentEntity(dashboard, new DashboardGroup());
    }
}
