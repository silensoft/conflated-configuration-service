package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class DataSourceMeasureServiceTests {

    @Tested
    private DataSourceMeasureService dataSourceMeasureService;

    @Injectable
    private MeasureStore measureStore;

    @Injectable
    private DataSourceStore dataSourceStore;

    @Test
    void getChildEntityStore() {
        assertEquals(dataSourceMeasureService.getChildEntityStore(), measureStore);
    }

    @Test
    void getParentEntityStore() {
        assertEquals(dataSourceMeasureService.getParentEntityStore(), dataSourceStore);
    }

    @Test
    void getParentEntitiesName() {
        assertEquals(dataSourceMeasureService.getParentEntitiesName(), "dataSource");
    }
}
