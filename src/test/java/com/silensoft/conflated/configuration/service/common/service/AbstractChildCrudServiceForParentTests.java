package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "DoubleBraceInitialization", "LawOfDemeter"})
public class AbstractChildCrudServiceForParentTests {
    private static final long ENTITY_ID = 1L;
    private static final long PARENT_ENTITY_ID = 2L;

    @Tested
    private TestChildCrudServiceForParent testChildServiceForParent;

    @Injectable
    private EntityStore<Chart> entityStore;

    @Injectable
    private EntityStore<Dashboard> parentEntityStore;

    @Mocked
    private Chart entityMock;

    @Mocked
    UriInfo uriInfoMock;

    @Mocked
    private Dashboard parentEntityMock;

    @Mocked
    UriBuilder uriBuilderMock;

    @Mocked
    private Response responseMock;

    @Mocked
    private URI uriMock;

    @Mocked
    private Response.ResponseBuilder responseBuilderMock;

    @Test
    void testCreateChildEntityForParent() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testChildServiceForParent.getParentEntityStore().readEntityWithId(PARENT_ENTITY_ID); result = parentEntityMock;
            parentEntityMock.getChildEntities(entityMock); result = Collections.singletonList(entityMock);
            uriInfoMock.getAbsolutePathBuilder(); result = uriBuilderMock;
            uriBuilderMock.path("{id}"); result = uriBuilderMock;
            //noinspection ResultOfMethodCallIgnored
            entityMock.getId(); result = ENTITY_ID;
            uriBuilderMock.build(ENTITY_ID); result = uriMock ;
            Response.created(uriMock); result = responseBuilderMock;
            responseBuilderMock.entity(entityMock); result = responseBuilderMock;
            responseBuilderMock.build(); result = responseMock;
        }};

        // WHEN
        final Response response = testChildServiceForParent.createChildEntityUnderParent(entityMock, uriInfoMock, PARENT_ENTITY_ID);

        // THEN
        new Verifications() {{
            parentEntityMock.addChildEntity(entityMock);
            testChildServiceForParent.getParentEntityStore().updateEntity(parentEntityMock);
            assertEquals(response, responseMock);
        }};

    }

    @Test
    void testAddChildEntityToParent() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
            testChildServiceForParent.getParentEntityStore().readEntityWithId(PARENT_ENTITY_ID); result = parentEntityMock;
            testChildServiceForParent.getChildEntityStore().readEntityWithId(ENTITY_ID); result = entityMock;
        }};

        // WHEN
        testChildServiceForParent.addChildEntityToParent(ENTITY_ID, PARENT_ENTITY_ID);

        // THEN
        new Verifications() {{
            parentEntityMock.addChildEntity(entityMock);
            testChildServiceForParent.getParentEntityStore().updateEntity(parentEntityMock);
        }};
    }

    @Test
    void testReadChildEntitiesOfParent() {
        // GIVEN
        new Expectations() {{
            testChildServiceForParent.getChildEntityStore().readEntitiesForParent("dashboard", PARENT_ENTITY_ID);
            result = Collections.singletonList(entityMock);
        }};

        // WHEN
        final List<Chart> entities = testChildServiceForParent.readChildEntitiesOfParent("", PARENT_ENTITY_ID);

        // THEN
        assertEquals(entities.size(), 1);
        assertEquals(entities.get(0), entityMock);
    }

    @Test
    void testRemoveChildEntityFromParent() throws EntityNotFoundException {
        // GIVEN
        new Expectations() {{
           testChildServiceForParent.getParentEntityStore().readEntityWithId(PARENT_ENTITY_ID); result = parentEntityMock;
           testChildServiceForParent.getChildEntityStore().readEntityWithId(ENTITY_ID); result = entityMock;
        }};

        // WHEN
        testChildServiceForParent.removeChildEntityFromParent(ENTITY_ID, PARENT_ENTITY_ID);

        // THEN
        new Verifications() {{
            parentEntityMock.removeChildEntity(entityMock);
            testChildServiceForParent.getParentEntityStore().updateEntity(parentEntityMock);
        }};
    }
}
