package com.silensoft.conflated.configuration.service.selectedfilter;

import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class SelectedFilterStoreImplTests {

    @Tested
    private SelectedFilterStoreImpl selectedFilterStore;

    @Injectable
    private EntityManager entityManager;

    @Test
    void testGetEntityClass() {
        assertEquals(selectedFilterStore.getEntityClass(), SelectedFilter.class);
    }
}
