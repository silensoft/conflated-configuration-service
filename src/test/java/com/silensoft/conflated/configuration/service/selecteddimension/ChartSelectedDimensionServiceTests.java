package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.chart.ChartStore;
import mockit.Injectable;
import mockit.Tested;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ChartSelectedDimensionServiceTests {

    @Tested
    private ChartSelectedDimensionService chartSelectedDimensionService;

    @Injectable
    private SelectedDimensionStore selectedDimensionStore;

    @Injectable
    private ChartStore chartStore;

    @Test
    void testGetChildEntityStore() {
        assertEquals(chartSelectedDimensionService.getChildEntityStore(), selectedDimensionStore);
    }

    @Test
    void testGetParentEntityStore() {
        assertEquals(chartSelectedDimensionService.getParentEntityStore(), chartStore);
    }

    @Test
    void testGetParentEntitiesName() {
        assertEquals(chartSelectedDimensionService.getParentEntitiesName(), "chart");
    }
}
