package com.silensoft.conflated.configuration.service.common.entity;

import mockit.Tested;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@SuppressWarnings("InstanceVariableOfConcreteClass")
public class ParentEntityTests {

    @Tested
    private TestParentEntity parentEntity;

    @Tested
    private ChildEntityOf<String> oldChildEntity;

    @Tested
    private ChildEntityOf<String> newChildEntity;

    @Test
    void testReplaceChildEntity() {
        // GIVEN
        parentEntity.addChildEntity(oldChildEntity);

        // WHEN
        parentEntity.replaceChildEntity(oldChildEntity, newChildEntity);

        // THEN
        assertEquals(parentEntity.getChildEntities(oldChildEntity).size(), 1);
        assertTrue(parentEntity.getChildEntities(oldChildEntity).contains(newChildEntity));
    }

    @Test
    void testUpdateChildEntitiesFromOtherChildEntities() {
        // GIVEN
        parentEntity.addChildEntity(oldChildEntity);
        final List<ChildEntityOf<String>> otherChildEntities = Collections.singletonList(newChildEntity);

        // WHEN
        parentEntity.updateChildEntitiesFromOtherChildEntities(otherChildEntities);

        // THEN
        assertEquals(parentEntity.getChildEntities(oldChildEntity).size(), 1);
        assertTrue(parentEntity.getChildEntities(oldChildEntity).contains(newChildEntity));
    }
}
