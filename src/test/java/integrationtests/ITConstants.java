package integrationtests;

public final class ITConstants {
    public static final String BASE_URL = "/conflated-configuration-service/v1/";

    private ITConstants() {}

}
