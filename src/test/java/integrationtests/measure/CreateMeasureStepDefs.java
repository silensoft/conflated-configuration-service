package integrationtests.measure;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"MethodParameterOfConcreteClass", "InstanceVariableOfConcreteClass"})
public class CreateMeasureStepDefs {
    private final TestContext testContext;

    public CreateMeasureStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("measure name is {string}")
    public void measureNameIs(final String name) {
        testContext.measure.setName(name);
    }

    @And("and expression is {string}")
    public void andExpressionIs(final String expression) {
        testContext.measure.setExpression(expression);
    }

    @And("unit is {string}")
    public void unitIs(final String unit) {
        testContext.measure.setUnit(unit);
    }

    @When("I create a measure")
    public void iCreateAMeasure() {
        testContext.response = given()
                .contentType(ContentType.JSON)
                .body(testContext.measure, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "measures");
        try {
            testContext.measureId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the measure given above with response code {int} {string}")
    public void iShouldGetTheMeasureGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.measureId))
                .body("name", equalTo(testContext.measure.getName()))
                .body("expression", equalTo(testContext.measure.getExpression()))
                .body("unit", equalTo(testContext.measure.getUnit()));
    }
}
