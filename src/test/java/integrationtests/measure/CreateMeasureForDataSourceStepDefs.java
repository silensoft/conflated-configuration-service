package integrationtests.measure;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateMeasureForDataSourceStepDefs {
    private final TestContext testContext;

    public CreateMeasureForDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new measure for the given data source")
    public void iCreateANewMeasureForTheGivenDataSource() {
        createMeasureForDataSourceWithId(testContext.dataSourceId);
        try {
            testContext.measureId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new measure for data source with id {int}")
    public void iCreateANewMeasureForDataSourceWithId(final int dataSourceId) {
        createMeasureForDataSourceWithId(dataSourceId);

    }

    private void createMeasureForDataSourceWithId(final int dataSourceId) {
        testContext.response = given()
                .pathParam("parentId", dataSourceId)
                .contentType(ContentType.JSON)
                .body(testContext.measure, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources/{parentId}/measures");
    }
}
