package integrationtests.datasource;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class ReadDataSourceStepDefs {
    private final TestContext testContext;

    public ReadDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I read the given data source")
    public void iReadTheGivenDataSource() {
        testContext.response = given().when().pathParam("id", testContext.dataSourceId).get(ITConstants.BASE_URL + "data-sources/{id}");
    }

    @Then("I should get the data source with added dimension with response code {int} {string}")
    public void iShouldGetTheDataSourceWithAddedDimension(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("dimensions.id", hasItems(testContext.dimensionId))
                .body("dimensions.name", hasItems(testContext.dimension.getName()));
    }

    @Then("I should get the data source with added filter with response code {int} {string}")
    public void iShouldGetTheDataSourceWithAddedFilterWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("filters.id", hasItems(testContext.filterId))
                .body("filters.name", hasItems(testContext.filter.getName()));
    }

    @Then("I should get the data source with added measure with response code {int} {string}")
    public void iShouldGetTheDataSourceWithAddedMeasureWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("measures.id", hasItems(testContext.measureId))
                .body("measures.name", hasItems(testContext.measure.getName()));
    }

    @Then("I should get the data source with added sort by with response code {int} {string}")
    public void iShouldGetTheDataSourceWithAddedSortByWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("sortBys.id", hasItems(testContext.sortById))
                .body("sortBys.name", hasItems(testContext.sortBy.getName()));
    }
}
