package integrationtests.datasource;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateDataSourceStepDefs {
    private final TestContext testContext;

    public CreateDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("data source's name is {string}")
    public void dataSourceSNameIs(final String name) {
        testContext.dataSource.setName(name);
    }

    @And("data source's jdbcDriverClass is {string}")
    public void dataSourceSJdbcDriverClassIs(final String jdbcDriverClass) {
        testContext.dataSource.setJdbcDriverClass(jdbcDriverClass);
    }

    @And("data source's jdbcUrl is {string}")
    public void dataSourceSJdbcUrlIs(final String jdbcUrl) {
        testContext.dataSource.setJdbcUrl(jdbcUrl);
    }

    @And("data source's userName is {string}")
    public void dataSourceSUserNameIs(final String userName) {
        testContext.dataSource.setUserName(userName);
    }

    @And("data source's password is {string}")
    public void dataSourceSPasswordIs(final String password) {
        testContext.dataSource.setPassword(password);
    }

    @And("data source's sqlStatement is {string}")
    public void dataSourceSSqlStatementIs(final String sqlStatement) {
        testContext.dataSource.setSqlStatement(sqlStatement);
    }

    @And("data source's dataRefreshIntervalValue is {int}")
    public void dataSourceSDataRefreshIntervalValueIs(final int dataRefreshIntervalValue) {
        testContext.dataSource.setDataRefreshIntervalValue((long) dataRefreshIntervalValue);
    }

    @And("data source's dataRefreshIntervalUnit is {string}")
    public void dataSourceSDataRefreshIntervalUnitIs(final String dataRefreshIntervalUnit) {
        testContext.dataSource.setDataRefreshIntervalUnit(dataRefreshIntervalUnit);
    }

    @And("data sources dataRefreshDelayValue is {int}")
    public void dataSourcesDataRefreshDelayValueIs(final int dataRefreshDelayValue) {
        testContext.dataSource.setDataRefreshDelayValue((long)dataRefreshDelayValue);
    }

    @And("data source's dataRefreshDelayUnit is {string}")
    public void dataSourceSDataRefreshDelayUnitIs(final String dataRefreshDelayUnit) {
        testContext.dataSource.setDataRefreshDelayUnit(dataRefreshDelayUnit);
    }

    @When("I create a new data source")
    public void iCreateANewDataSource() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.dataSource, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources");
        try {
            testContext.dataSourceId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the data source given above with response code {int} {string}")
    public void iShouldGetTheDataSourceGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        //noinspection NumericCastThatLosesPrecision
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.dataSourceId))
                .body("name", equalTo(testContext.dataSource.getName()))
                .body("jdbcDriverClass", equalTo(testContext.dataSource.getJdbcDriverClass()))
                .body("jdbcUrl", equalTo(testContext.dataSource.getJdbcUrl()))
                .body("userName", equalTo(testContext.dataSource.getUserName()))
                .body("password", equalTo(testContext.dataSource.getPassword()))
                .body("sqlStatement", equalTo(testContext.dataSource.getSqlStatement()))
                .body("dataRefreshIntervalValue", equalTo((int)testContext.dataSource.getDataRefreshIntervalValue()))
                .body("dataRefreshIntervalUnit", equalTo(testContext.dataSource.getDataRefreshIntervalUnit()))
                .body("dataRefreshDelayValue", equalTo((int)testContext.dataSource.getDataRefreshDelayValue()))
                .body("dataRefreshDelayUnit", equalTo(testContext.dataSource.getDataRefreshDelayUnit()));
    }
}
