package integrationtests.filter;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateFilterStepDefs {
    private final TestContext testContext;

    public CreateFilterStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("filter name is {string}")
    public void filterNameIs(final String filterName) {
        testContext.filter.setName(filterName);
    }

    @And("filter expression is {string}")
    public void filterExpressionIs(final String filterExpression) {
        testContext.filter.setExpression(filterExpression);
    }

    @And("filter unit is {string}")
    public void filterUnitIs(final String unit) {
        testContext.filter.setUnit(unit);
    }

    @And("filter type is {string}")
    public void filterTypeIs(final String type) {
        testContext.filter.setType(type);
    }

    @When("I create a filter")
    public void iCreateAFilter() {
        testContext.response = given()
                .contentType(ContentType.JSON)
                .body(testContext.filter, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "filters");
        try {
            testContext.filterId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the filter given above with response code {int} {string}")
    public void iShouldGetTheFilterGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.filterId))
                .body("name", equalTo(testContext.filter.getName()))
                .body("expression", equalTo(testContext.filter.getExpression()))
                .body("unit", equalTo(testContext.filter.getUnit()))
                .body("type", equalTo(testContext.filter.getType()));
    }
}
