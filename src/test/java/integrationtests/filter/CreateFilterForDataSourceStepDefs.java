package integrationtests.filter;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateFilterForDataSourceStepDefs {
    private final TestContext testContext;

    public CreateFilterForDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new filter for the given data source")
    public void iCreateANewFilterForTheGivenDataSource() {
        createFilterForDataSourceWithId(testContext.dataSourceId);
        try {
            testContext.filterId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new filter for data source with id {int}")
    public void iCreateANewFilterForDataSourceWithId(final int dataSourceId) {
        createFilterForDataSourceWithId(dataSourceId);
    }

    private void createFilterForDataSourceWithId(final int dataSourceId) {
        testContext.response = given()
                .pathParam("parentId", dataSourceId)
                .contentType(ContentType.JSON)
                .body(testContext.filter, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources/{parentId}/filters");
    }
}
