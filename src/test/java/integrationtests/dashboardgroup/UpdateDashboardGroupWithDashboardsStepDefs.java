package integrationtests.dashboardgroup;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class UpdateDashboardGroupWithDashboardsStepDefs {
    private final TestContext testContext;

    public UpdateDashboardGroupWithDashboardsStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @And("dashboard group's dashboards is dashboard given above")
    public void dashboardGroupSDashboardsIsDashboardGivenAbove() {
        testContext.dashboardGroup.setDashboards(Collections.singletonList(testContext.dashboard));
    }

    @When("I update given dashboard group")
    public void iUpdateGivenDashboardGroup() {
        testContext.response = given().when()
                .pathParam("id", testContext.dashboardGroupId)
                .contentType(ContentType.JSON)
                .body(testContext.dashboardGroup, ObjectMapperType.GSON)
                .put(ITConstants.BASE_URL + "dashboard-groups/{id}");
    }

    @Then("I should get the dashboard group given above with updated dashboards with response code {int} {string}")
    public void iShouldGetTheDashboardGroupGivenAboveWithUpdatedDashboardsWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("dashboards.name", hasItems(testContext.dashboard.getName()))
                .body("dashboards.theme", hasItems(testContext.dashboard.getTheme()));
    }
}
