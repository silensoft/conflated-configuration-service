package integrationtests.dashboardgroup;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateDashboardGroupStepDefs {
    private final TestContext testContext;

    public CreateDashboardGroupStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("dashboard group's name is {string}")
    public void dashboardGroupSNameIs(final String name) {
        testContext.dashboardGroup.setName(name);
    }

    @And("I create a new dashboard group")
    public void iCreateANewDashboardGroup() {
        testContext.response = given()
                .contentType("application/json")
                .body(testContext.dashboardGroup, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "dashboard-groups");
        try {
            testContext.dashboardGroupId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }
}
