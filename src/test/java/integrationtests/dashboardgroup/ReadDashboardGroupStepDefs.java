package integrationtests.dashboardgroup;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class ReadDashboardGroupStepDefs {
    private final TestContext testContext;

    public ReadDashboardGroupStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I read the given dashboard group")
    public void iReadTheGivenDashboardGroup() {
        testContext.response = given().when().pathParam("id", testContext.dashboardGroupId).get(ITConstants.BASE_URL + "dashboard-groups/{id}");
    }

    @Then("I should get the dashboard group with added dashboard with response code {int} {string}")
    public void iShouldGetTheDashboardGroupWithAddedDashboardWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("dashboards.id", hasItems(testContext.dashboardId))
                .body("dashboards.name", hasItems(testContext.dashboard.getName()));
    }

    @Then("I should get the dashboard group given above without dashboards with response code {int} {string}")
    public void iShouldGetTheDashboardGroupGivenAboveWithoutDashboardsWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("dashboards.id", hasSize(0));
    }
}
