package integrationtests.common;

import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CommonStepDefs {
    private final TestContext testContext;

    public CommonStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Then("I should get a response with response code {int} {string}")
    public void iShouldGetAResponseWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode);
    }

    @And("response body should contain {string} entries for following fields")
    public void responseBodyShouldContainEntriesForFollowingFields(final String errorEntryText, final List<String> fields) {
        testContext.response.then()
                .assertThat()
                .body("", hasItems(fields.stream().map(field -> field + ' ' + errorEntryText).toArray()));
    }
}
