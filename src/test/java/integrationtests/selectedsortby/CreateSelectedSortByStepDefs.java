package integrationtests.selectedsortby;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedSortByStepDefs {
    private final TestContext testContext;

    public CreateSelectedSortByStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("selected sort by's measureOrDimensionName is {string}")
    public void selectedSortBySMeasureOrDimensionNameIs(final String measureOrDimensionName) {
        testContext.selectedSortBy.setMeasureOrDimensionName(measureOrDimensionName);
    }

    @And("selected sort by's measureOrDimensionExpression is {string}")
    public void selectedSortBySMeasureOrDimensionExpressionIs(final String measureOrDimensionExpression) {
        testContext.selectedSortBy.setMeasureOrDimensionExpression(measureOrDimensionExpression);
    }

    @And("selected sort by's type is {string}")
    public void selectedSortBySTypeIs(final String type) {
        testContext.selectedSortBy.setType(type);
    }

    @And("selected sort by's aggregationFunction is {string}")
    public void selectedSortBySAggregationFunctionIs(final String aggregationFunction) {
        testContext.selectedSortBy.setAggregationFunction(aggregationFunction);
    }

    @And("selected sort by's timeSortOption is {string}")
    public void selectedSortBySTimeSortOptionIs(final String timeSortOption) {
        testContext.selectedSortBy.setTimeSortOption(timeSortOption);
    }

    @And("selected sort by's sortDirection is {string}")
    public void selectedSortBySSortDirectionIs(final String sortDirection) {
        testContext.selectedSortBy.setSortDirection(sortDirection);
    }

    @And("selected sort by's sortByDataScopeType is {string}")
    public void selectedSortBySSortByDataScopeTypeIs(final String sortByDataScopeType) {
        testContext.selectedSortBy.setSortByDataScopeType(sortByDataScopeType);
    }

    @And("selected sort by's defaultSortByType is {string}")
    public void selectedSortBySDefaultSortByTypeIs(final String defaultSortByType) {
        testContext.selectedSortBy.setDefaultSortByType(defaultSortByType);
    }

    @When("I create a selected sort by")
    public void iCreateASelectedSortBy() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.selectedSortBy, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "selected-sort-bys");
        try {
            testContext.selectedSortById = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the selected sort by given above with response code {int} {string}")
    public void iShouldGetTheSelectedSortByGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.selectedSortById))
                .body("measureOrDimensionName", equalTo(testContext.selectedSortBy.getMeasureOrDimensionName()))
                .body("measureOrDimensionExpression", equalTo(testContext.selectedSortBy.getMeasureOrDimensionExpression()))
                .body("aggregationFunction", equalTo(testContext.selectedSortBy.getAggregationFunction()))
                .body("timeSortOption", equalTo(testContext.selectedSortBy.getTimeSortOption()))
                .body("sortDirection", equalTo(testContext.selectedSortBy.getSortDirection()))
                .body("sortByDataScopeType", equalTo(testContext.selectedSortBy.getSortByDataScopeType()))
                .body("defaultSortByType", equalTo(testContext.selectedSortBy.getDefaultSortByType()));
    }
}
