package integrationtests.selectedsortby;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedSortByForChartStepDefs {
    private final TestContext testContext;

    public CreateSelectedSortByForChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new selected sort by for the given chart")
    public void iCreateANewSelectedSortByForTheGivenChart() {
        createSelectedSortByForChartWithId(testContext.chartId);
        try {
            testContext.selectedSortById = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new selected sort by for chart with id {int}")
    public void iCreateANewSelectedSortByForChartWithId(final int chartId) {
        createSelectedSortByForChartWithId(chartId);
    }

    private void createSelectedSortByForChartWithId(final int chartId) {
        testContext.response = given()
                .pathParam("parentId", chartId)
                .contentType(ContentType.JSON)
                .body(testContext.selectedSortBy, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "charts/{parentId}/selected-sort-bys");
    }
}
