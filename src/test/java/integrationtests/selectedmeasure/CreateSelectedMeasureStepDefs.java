package integrationtests.selectedmeasure;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedMeasureStepDefs {
    private final TestContext testContext;

    public CreateSelectedMeasureStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("selected measure's name is {string}")
    public void selectedMeasureSNameIs(final String name) {
        testContext.selectedMeasure.setName(name);
    }

    @And("selected measure's expression is {string}")
    public void selectedMeasureSExpressionIs(final String expression) {
        testContext.selectedMeasure.setExpression(expression);
    }

    @And("selected measure's unit is {string}")
    public void selectedMeasureSUnitIs(final String unit) {
        testContext.selectedMeasure.setUnit(unit);
    }

    @And("selected measures's aggregationFunction is {string}")
    public void selectedMeasuresSAggregationFunctionIs(final String aggregationFunction) {
        testContext.selectedMeasure.setAggregationFunction(aggregationFunction);
    }

    @And("selected measure's visualizationType is {string}")
    public void selectedMeasureSVisualizationTypeIs(final String visualizationType) {
        testContext.selectedMeasure.setVisualizationType(visualizationType);
    }

    @And("selected measure's visualizationColor is {string}")
    public void selectedMeasureSVisualizationColorIs(final String visualizationColor) {
        testContext.selectedMeasure.setVisualizationColor(visualizationColor);
    }

    @When("I create a selected measure")
    public void iCreateASelectedMeasure() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.selectedMeasure, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "selected-measures");
        try {
            testContext.selectedMeasureId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the selected measure given above with response code {int} {string}")
    public void iShouldGetTheSelectedMeasureGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.selectedMeasureId))
                .body("name", equalTo(testContext.selectedMeasure.getName()))
                .body("expression", equalTo(testContext.selectedMeasure.getExpression()))
                .body("unit", equalTo(testContext.selectedMeasure.getUnit()))
                .body("aggregationFunction", equalTo(testContext.selectedMeasure.getAggregationFunction()))
                .body("visualizationType", equalTo(testContext.selectedMeasure.getVisualizationType()))
                .body("visualizationColor", equalTo(testContext.selectedMeasure.getVisualizationColor()));
    }
}
