package integrationtests.selectedmeasure;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedMeasureForChartStepDefs {
    private final TestContext testContext;

    public CreateSelectedMeasureForChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new selected measure for the given chart")
    public void iCreateANewSelectedMeasureForTheGivenChart() {
        createSelectedMeasureForChartWithId(testContext.chartId);
        try {
            testContext.selectedMeasureId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new selected measure for chart with id {int}")
    public void iCreateANewSelectedMeasureForChartWithId(final int chartId) {
        createSelectedMeasureForChartWithId(chartId);
    }

    private void createSelectedMeasureForChartWithId(final int chartId) {
        testContext.response = given()
                .pathParam("parentId", chartId)
                .contentType(ContentType.JSON)
                .body(testContext.selectedMeasure, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "charts/{parentId}/selected-measures");
    }
}
