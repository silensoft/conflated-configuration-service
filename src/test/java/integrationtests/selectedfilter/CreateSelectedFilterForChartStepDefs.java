package integrationtests.selectedfilter;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedFilterForChartStepDefs {
    private final TestContext testContext;

    public CreateSelectedFilterForChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new selected filter for the given chart")
    public void iCreateANewSelectedFilterForTheGivenChart() {
        createSelectedFilterForChartWithId(testContext.chartId);
        try {
            testContext.selectedFilterId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new selected filter for chart with id {int}")
    public void iCreateANewSelectedFilterForChartWithId(final int chartId) {
        createSelectedFilterForChartWithId(chartId);
    }

    private void createSelectedFilterForChartWithId(final int chartId) {
        testContext.response = given()
                .pathParam("parentId", chartId)
                .contentType(ContentType.JSON)
                .body(testContext.selectedFilter, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "charts/{parentId}/selected-filters");
    }

}
