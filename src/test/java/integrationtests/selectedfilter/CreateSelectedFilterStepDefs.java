package integrationtests.selectedfilter;

import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilter;
import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedFilterStepDefs {
    private final TestContext testContext;

    public CreateSelectedFilterStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("selected filter's measureOrDimensionName is {string}")
    public void selectedFilterSMeasureOrDimensionNameIs(final String measureOrDimensionName) {
        testContext.selectedFilter.setMeasureOrDimensionName(measureOrDimensionName);
    }

    @And("selected filter's measureOrDimensionExpression is {string}")
    public void selectedFilterSMeasureOrDimensionExpressionIs(final String measureOrDimensionExpression) {
        testContext.selectedFilter.setMeasureOrDimensionExpression(measureOrDimensionExpression);
    }

    @And("selected filter's type is {string}")
    public void selectedFilterSTypeIs(final String type) {
        testContext.selectedFilter.setType(type);
    }

    @And("selected filter's aggregationFunction is {string}")
    public void selectedFilterSAggregationFunctionIs(final String aggregationFunction) {
        testContext.selectedFilter.setAggregationFunction(aggregationFunction);
    }

    @And("selected filter's filterExpression is {string}")
    public void selectedFilterSFilterExpressionIs(final String filterExpression) {
        testContext.selectedFilter.setFilterExpression(filterExpression);
    }

    @And("selected filter's filterInputType is {string}")
    public void selectedFilterSFilterInputTypeIs(final String filterInputType) {
        testContext.selectedFilter.setFilterInputType(filterInputType);
    }

    @And("selected filter's filterDataScopeType is {string}")
    public void selectedFilterSFilterDataScopeTypeIs(final String filterDataScopeType) {
        testContext.selectedFilter.setFilterDataScopeType(filterDataScopeType);
    }

    @And("selected filter's allowedDimensionFilterInputTypes is {string}")
    public void selectedFilterSAllowedDimensionFilterInputTypesIs(final String allowedDimensionFilterInputTypes) {
        testContext.selectedFilter.setAllowedDimensionFilterInputTypes(allowedDimensionFilterInputTypes);
    }

    @When("I create a selected filter")
    public void iCreateASelectedFilter() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.selectedFilter, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "selected-filters");
        try {
            testContext.selectedFilterId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the selected filter given above with response code {int} {string}")
    public void iShouldGetTheSelectedFilterGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.selectedFilterId))
                .body("measureOrDimensionName", equalTo(testContext.selectedFilter.getMeasureOrDimensionName()))
                .body("measureOrDimensionExpression", equalTo(testContext.selectedFilter.getMeasureOrDimensionExpression()))
                .body("type", equalTo(testContext.selectedFilter.getType()))
                .body("filterExpression", equalTo(testContext.selectedFilter.getFilterExpression()))
                .body("filterInputType", equalTo(testContext.selectedFilter.getFilterInputType()))
                .body("filterDataScopeType", equalTo(testContext.selectedFilter.getFilterDataScopeType()))
                .body("allowedDimensionFilterInputTypes", equalTo(testContext.selectedFilter.getAllowedDimensionFilterInputTypes()));
    }
}
