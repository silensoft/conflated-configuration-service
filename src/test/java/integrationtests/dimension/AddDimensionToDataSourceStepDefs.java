package integrationtests.dimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class AddDimensionToDataSourceStepDefs {
    private final TestContext testContext;

    public AddDimensionToDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I add given dimension for the given data source")
    public void iAddGivenDimensionForTheGivenDataSource() {
        testContext.response = given()
                .pathParam("parentId", testContext.dataSourceId)
                .pathParam("id", testContext.dimensionId)
                .contentType(ContentType.JSON)
                .body(testContext.dimension, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources/{parentId}/dimensions/{id}");
    }
}
