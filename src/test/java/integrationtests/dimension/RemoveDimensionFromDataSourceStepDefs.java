package integrationtests.dimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class RemoveDimensionFromDataSourceStepDefs {
    private final TestContext testContext;

    public RemoveDimensionFromDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I remove the given dimension from data source")
    public void iRemoveTheGivenDimensionFromDataSource() {
        testContext.response = given()
                .pathParam("parentId", testContext.dataSourceId)
                .pathParam("id", testContext.dimensionId)
                .delete(ITConstants.BASE_URL + "data-sources/{parentId}/dimensions/{id}");
    }

    @Then("I should get the data source without the given dimension with response code {int} {string}")
    public void iShouldGetTheDataSourceWithoutTheGivenDimensionWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("dimensions", hasSize(0));
    }
}
