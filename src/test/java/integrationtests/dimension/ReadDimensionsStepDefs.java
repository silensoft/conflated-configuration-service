package integrationtests.dimension;

import integrationtests.TestContext;
import integrationtests.ITConstants;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SuppressWarnings({"MethodParameterOfConcreteClass", "InstanceVariableOfConcreteClass"})
public class ReadDimensionsStepDefs {
    private final TestContext testContext;

    public ReadDimensionsStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @SuppressWarnings("MethodMayBeStatic")
    @Given("All dimensions are removed from database")
    public void allDimensionsAreRemovedFromDatabase() {
        given().when().delete(ITConstants.BASE_URL + "dimensions");
    }

    @When("I read dimensions")
    public void iReadDimensions() {
        testContext.response = given().when().get(ITConstants.BASE_URL + "dimensions");
    }

    @Then("I should get a list of dimensions with the dimension given above")
    public void iShouldGetAListOfDimensionsWithTheDimensionGivenAbove() {
        testContext.response.then()
                .assertThat()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("name", hasItems(testContext.dimension.getName()))
                .body("expression", hasItems(testContext.dimension.getExpression()))
                .body("isTimestamp", hasItems(testContext.dimension.getIsTimestamp()))
                .body("isDate", hasItems(testContext.dimension.getIsDate()))
                .body("isString", hasItems(testContext.dimension.getIsString()));
    }
}
