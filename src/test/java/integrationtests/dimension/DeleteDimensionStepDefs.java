package integrationtests.dimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class DeleteDimensionStepDefs {
    private final TestContext testContext;

    public DeleteDimensionStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I delete dimension created above")
    public void iDeleteDimensionCreatedAbove() {
        testContext.response = given().when().pathParam("id", testContext.dimensionId).delete(ITConstants.BASE_URL + "dimensions/{id}");
    }
}
