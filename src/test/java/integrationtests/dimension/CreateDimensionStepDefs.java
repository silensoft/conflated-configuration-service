package integrationtests.dimension;

import integrationtests.TestContext;
import integrationtests.ITConstants;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"MethodParameterOfConcreteClass", "InstanceVariableOfConcreteClass"})
public class CreateDimensionStepDefs {
    private final TestContext testContext;

    public CreateDimensionStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("dimension name is {string}")
    public void dimensionNameIs(final String name) {
        testContext.dimension.setName(name);
    }

    @And("dimension expression is {string}")
    public void dimensionExpressionIs(final String expression) {
        testContext.dimension.setExpression(expression);
    }

    @And("dimension isTimestamp is false")
    public void dimensionIsTimestampIsFalse() {
        testContext.dimension.setIsTimestamp(false);
    }

    @And("dimension isDate is false")
    public void dimensionIsDateIsFalse() {
        testContext.dimension.setIsDate(false);
    }

    @And("dimension isString is false")
    public void dimensionIsStringIsFalse() {
        testContext.dimension.setIsString(false);
    }

    @When("I create a new dimension")
    public void iCreateANewDimension() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.dimension, ObjectMapperType.GSON).when().post(ITConstants.BASE_URL + "dimensions");
        try {
            testContext.dimensionId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the dimension given above with response code {int} {string}")
    public void iShouldGetTheDimensionGivenAbove(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.dimensionId))
                .body("name", equalTo(testContext.dimension.getName()))
                .body("expression", equalTo(testContext.dimension.getExpression()))
                .body("isTimestamp", equalTo(testContext.dimension.getIsTimestamp()))
                .body("isDate", equalTo(testContext.dimension.getIsDate()))
                .body("isString", equalTo(testContext.dimension.getIsString()));
    }

    @Given("dimension id is set to above created dimension id")
    public void dimensionIdIsSetToAboveCreatedDimensionId() {
        testContext.dimension.setId(testContext.dimensionId);
    }
}
