package integrationtests.dimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateDimensionForDataSourceStepDefs {
    private final TestContext testContext;

    public CreateDimensionForDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new dimension for the given data source")
    public void iCreateANewDimensionForDataSource() {
        createDimensionForDataSourceWithId(testContext.dataSourceId);
        try {
            testContext.dimensionId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new dimension for data source with id {int}")
    public void iCreateANewDimensionForDataSourceWithId(final int dataSourceId) {
        createDimensionForDataSourceWithId(dataSourceId);
    }

    private void createDimensionForDataSourceWithId(final int dataSourceId) {
        testContext.response = given()
                .pathParam("parentId", dataSourceId)
                .contentType(ContentType.JSON)
                .body(testContext.dimension, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources/{parentId}/dimensions");
    }
}
