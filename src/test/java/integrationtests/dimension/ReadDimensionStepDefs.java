package integrationtests.dimension;

import integrationtests.TestContext;
import integrationtests.ITConstants;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"MethodParameterOfConcreteClass", "InstanceVariableOfConcreteClass"})
public class ReadDimensionStepDefs {
    private final TestContext testContext;

    public ReadDimensionStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I read dimension with created dimension id")
    public void iReadDimensionWithCreatedDimensionId() {
        testContext.response = given().when().pathParam("id", testContext.dimensionId).get(ITConstants.BASE_URL + "dimensions/{id}");
    }
}
