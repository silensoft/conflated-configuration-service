package integrationtests.dimension;

import integrationtests.TestContext;
import integrationtests.ITConstants;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class UpdateDimensionStepDefs {
    private final TestContext testContext;

    public UpdateDimensionStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I update dimension name to {string} for the above created dimension")
    public void iUpdateDimensionNameToForTheAboveCreatedDimension(final String newDimensionName) {
        testContext.dimension.setName(newDimensionName);
        testContext.dimension.setId(testContext.dimensionId);
        testContext.response = given().when()
                .pathParam("id", testContext.dimensionId)
                .contentType(ContentType.JSON)
                .body(testContext.dimension, ObjectMapperType.GSON)
                .put(ITConstants.BASE_URL + "dimensions/{id}");
    }
}
