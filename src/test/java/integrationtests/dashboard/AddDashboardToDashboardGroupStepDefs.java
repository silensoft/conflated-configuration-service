package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class AddDashboardToDashboardGroupStepDefs {
    private final TestContext testContext;

    public AddDashboardToDashboardGroupStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @And("I add given dashboard to given dashboard group")
    public void iAddGivenDashboardToGivenDashboardGroup() {
        testContext.response = given()
                .pathParam("parentId", testContext.dashboardGroupId)
                .pathParam("id", testContext.dashboardId)
                .contentType(ContentType.JSON)
                .body(testContext.dashboard, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "dashboard-groups/{parentId}/dashboards/{id}");
    }
}
