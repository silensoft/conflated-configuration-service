package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class DeleteDashboardStepDefs {
    private final TestContext testContext;

    public DeleteDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I delete the given dashboard")
    public void iDeleteTheGivenDashboard() {
        testContext.response = given().when().pathParam("id", testContext.dashboardId).delete(ITConstants.BASE_URL + "dashboards/{id}");
    }
}
