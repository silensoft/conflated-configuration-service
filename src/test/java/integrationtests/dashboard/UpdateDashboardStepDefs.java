package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class UpdateDashboardStepDefs {
    private final TestContext testContext;

    public UpdateDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I update given dashboard")
    public void iUpdateGivenDashboard() {
        testContext.response = given().when()
                .pathParam("id", testContext.dashboardId)
                .contentType(ContentType.JSON)
                .body(testContext.dashboard, ObjectMapperType.GSON)
                .put(ITConstants.BASE_URL + "dashboards/{id}");
    }
}
