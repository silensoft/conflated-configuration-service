package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateDashboardStepDefs {
    private final TestContext testContext;

    public CreateDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("dashboard's name is {string}")
    public void dashboardSNameIs(final String name) {
        testContext.dashboard.setName(name);
    }

    @And("dashboard's theme is {string}")
    public void dashboardSThemeIs(final String theme) {
        testContext.dashboard.setTheme(theme);
    }

    @When("I create a new dashboard")
    public void iCreateANewDashboard() {
        testContext.response = given()
                .contentType("application/json")
                .body(testContext.dashboard, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "dashboards");
        try {
            testContext.dashboardId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the dashboard given above with response code {int} {string}")
    public void iShouldGetTheDashboardGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.dashboardId))
                .body("name", equalTo(testContext.dashboard.getName()))
                .body("theme", equalTo(testContext.dashboard.getTheme()));
    }
}
