package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class ReadDashboardStepDefs {
    private final TestContext testContext;

    public ReadDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I read the given dashboard")
    public void iReadTheGivenDashboard() {
        testContext.response = given().when().pathParam("id", testContext.dashboardId).get(ITConstants.BASE_URL + "dashboards/{id}");
    }

    @Then("I should get the dashboard with added chart with response code {int} {string}")
    public void iShouldGetTheDashboardWithAddedChartWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("charts.id", hasItems(testContext.chartId))
                .body("charts.layoutId", hasItems(testContext.chart.getLayoutId()));
    }

    @Then("I should get the dashboard with added grid item with response code {int} {string}")
    public void iShouldGetTheDashboardWithAddedGridItemWithResponseCode(final int responseCode, final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("layout.id", hasItems(testContext.gridItemId))
                .body("layout.i", hasItems(testContext.gridItem.getI()));
    }
}
