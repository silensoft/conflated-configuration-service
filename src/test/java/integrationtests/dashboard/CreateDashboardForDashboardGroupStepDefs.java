package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateDashboardForDashboardGroupStepDefs {
    private final TestContext testContext;

    public CreateDashboardForDashboardGroupStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new dashboard for the given dashboard group")
    public void iCreateANewDashboardForTheGivenDashboardGroup() {
        createDashboardForDashboardGroupWithId(testContext.dashboardGroupId);
        try {
            testContext.dashboardId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new dashboard for dashboard group with id {int}")
    public void iCreateANewDashboardForDashboardGroupWithId(final int dashboardGroupId) {
        createDashboardForDashboardGroupWithId(dashboardGroupId);
    }

    private void createDashboardForDashboardGroupWithId(final int dashboardGroupId) {
        testContext.response = given()
                .pathParam("parentId", dashboardGroupId)
                .contentType(ContentType.JSON)
                .body(testContext.dashboard, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "dashboard-groups/{parentId}/dashboards");
    }
}
