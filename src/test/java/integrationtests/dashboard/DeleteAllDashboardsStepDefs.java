package integrationtests.dashboard;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class DeleteAllDashboardsStepDefs {
    private final TestContext testContext;

    public DeleteAllDashboardsStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I delete all dashboards")
    public void iDeleteAllDashboards() {
        testContext.response = given().when().delete(ITConstants.BASE_URL + "dashboards");
    }
}
