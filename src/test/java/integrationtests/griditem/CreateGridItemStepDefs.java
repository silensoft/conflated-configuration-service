package integrationtests.griditem;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateGridItemStepDefs {
    private final TestContext testContext;

    public CreateGridItemStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("grid item i is {string}")
    public void gridItemIIs(final String i) {
        testContext.gridItem.setI(i);
    }

    @And("grid item x is {int}")
    public void gridItemXIs(final int x) {
        testContext.gridItem.setX(x);
    }

    @And("grid item y is {int}")
    public void gridItemYIs(final int y) {
        testContext.gridItem.setY(y);
    }

    @And("grid item w is {int}")
    public void gridItemWIs(final int w) {
        testContext.gridItem.setW(w);
    }

    @And("grid item h is {int}")
    public void gridItemHIs(final int h) {
        testContext.gridItem.setH(h);
    }

    @When("I create a grid item")
    public void iCreateAGridItem() {
        testContext.response = given()
                .contentType(ContentType.JSON)
                .body(testContext.gridItem, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "grid-items");
        try {
            testContext.gridItemId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the grid item given above with response code {int} {string}")
    public void iShouldGetTheGridItemGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.gridItemId))
                .body("i", equalTo(testContext.gridItem.getI()))
                .body("x", equalTo(testContext.gridItem.getX()))
                .body("w", equalTo(testContext.gridItem.getW()))
                .body("h", equalTo(testContext.gridItem.getH()));
    }
}
