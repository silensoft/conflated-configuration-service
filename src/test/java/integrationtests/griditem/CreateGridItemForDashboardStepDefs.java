package integrationtests.griditem;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateGridItemForDashboardStepDefs {
    private final TestContext testContext;

    public CreateGridItemForDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new grid item for the given dashboard")
    public void iCreateANewGridItemForTheGivenDashboard() {
        createGridItemForDashboardWithId(testContext.dashboardId);
        try {
            testContext.gridItemId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new grid item for dashboard with id {int}")
    public void iCreateANewGridItemForDashboardWithId(final int dashboardId) {
        createGridItemForDashboardWithId(dashboardId);
    }

    private void createGridItemForDashboardWithId(final int dashboardId) {
        testContext.response = given()
                .pathParam("parentId", dashboardId)
                .contentType(ContentType.JSON)
                .body(testContext.gridItem, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "dashboards/{parentId}/grid-items");
    }
}
