package integrationtests.chart;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class ReadChartStepDefs {
    private final TestContext testContext;

    public ReadChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I read the given chart")
    public void iReadTheGivenChart() {
        testContext.response = given().when().pathParam("id", testContext.chartId).get(ITConstants.BASE_URL + "charts/{id}");
    }

    @Then("I should get the chart with added selected dimension with response code {int} {string}")
    public void iShouldGetTheChartWithAddedSelectedDimensionWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("selectedDimensions.id", hasItems(testContext.selectedDimensionId))
                .body("selectedDimensions.name", hasItems(testContext.selectedDimension.getName()));
    }

    @Then("I should get the chart with added selected filter with response code {int} {string}")
    public void iShouldGetTheChartWithAddedSelectedFilterWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("selectedFilters.id", hasItems(testContext.selectedFilterId))
                .body("selectedFilters.measureOrDimensionName", hasItems(testContext.selectedFilter.getMeasureOrDimensionName()));
    }

    @Then("I should get the chart with added selected measure with response code {int} {string}")
    public void iShouldGetTheChartWithAddedSelectedMeasureWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("selectedMeasures.id", hasItems(testContext.selectedMeasureId))
                .body("selectedMeasures.name", hasItems(testContext.selectedMeasure.getName()));
    }

    @Then("I should get the chart with added selected sort by with response code {int} {string}")
    public void iShouldGetTheChartWithAddedSelectedSortByWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("selectedSortBys.id", hasItems(testContext.selectedSortById))
                .body("selectedSortBys.measureOrDimensionName", hasItems(testContext.selectedSortBy.getMeasureOrDimensionName()));
    }
}
