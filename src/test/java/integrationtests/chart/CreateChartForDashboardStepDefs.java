package integrationtests.chart;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateChartForDashboardStepDefs {
    private final TestContext testContext;

    public CreateChartForDashboardStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new chart for the given dashboard")
    public void iCreateANewChartForTheGivenDashboard() {
        createNewChartForDashboardWithId(testContext.dashboardId);
        try {
            testContext.chartId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new chart for dashboard with id {int}")
    public void iCreateANewChartForDashboardWithId(final int dashboardId) {
        createNewChartForDashboardWithId(dashboardId);
    }

    private void createNewChartForDashboardWithId(final int dashboardId) {
        testContext.response = given()
                .pathParam("parentId", dashboardId)
                .contentType("application/json")
                .body(testContext.chart, ObjectMapperType.GSON)
                .when().post(ITConstants.BASE_URL + "dashboards/{parentId}/charts");
    }
}
