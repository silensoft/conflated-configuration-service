package integrationtests.chart;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateChartStepDefs {
    private final TestContext testContext;

    public CreateChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("chart layout id is {string}")
    public void setChartLayoutId(final String layoutId) {
        testContext.chart.setLayoutId(layoutId);
    }

    @Given("chart type is {string}")
    public void setChartType(final String chartType) {
        testContext.chart.setChartType(chartType);
    }

    @Given("X-axis categories shown count is {int}")
    public void setXAxisCategoriesShownCount(final Integer xAxisCategoriesShownCount) {
        testContext.chart.setxAxisCategoriesShownCount(xAxisCategoriesShownCount);
    }

    @Given("fetchedRowCount is {int}")
    public void setFetchedRowCount(final Integer fetchedRowCount) {
        testContext.chart.setFetchedRowCount(fetchedRowCount);
    }

    @When("I create a new chart")
    public void createNewChart() {
        testContext.response = given().contentType("application/json").body(testContext.chart, ObjectMapperType.GSON).when().post(ITConstants.BASE_URL + "charts");
        try {
            testContext.chartId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the chart given above with response code {int} {string}")
    public void iShouldGetTheChartGivenAbove(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.chartId))
                .body("layoutId", equalTo(testContext.chart.getLayoutId()))
                .body("chartType", equalTo(testContext.chart.getChartType()))
                .body("xAxisCategoriesShownCount", equalTo(testContext.chart.getxAxisCategoriesShownCount()))
                .body("fetchedRowCount", equalTo(testContext.chart.getFetchedRowCount()));
    }
}
