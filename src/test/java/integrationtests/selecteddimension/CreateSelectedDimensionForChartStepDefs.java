package integrationtests.selecteddimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSelectedDimensionForChartStepDefs {
    private final TestContext testContext;

    public CreateSelectedDimensionForChartStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new selected dimension for the given chart")
    public void iCreateANewSelectedDimensionForTheGivenChart() {
        createSelectedDimensionForChartWithId(testContext.chartId);
        try {
            testContext.selectedDimensionId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new selected dimension for chart with id {int}")
    public void iCreateANewSelectedDimensionForChartWithId(final int chartId) {
        createSelectedDimensionForChartWithId(chartId);
    }

    private void createSelectedDimensionForChartWithId(final int chartId) {
        testContext.response = given()
                .pathParam("parentId", chartId)
                .contentType(ContentType.JSON)
                .body(testContext.selectedDimension, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "charts/{parentId}/selected-dimensions");
    }
}
