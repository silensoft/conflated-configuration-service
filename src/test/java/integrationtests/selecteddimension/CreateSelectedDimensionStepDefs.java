package integrationtests.selecteddimension;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"MethodParameterOfConcreteClass", "InstanceVariableOfConcreteClass"})
public class CreateSelectedDimensionStepDefs {
    private final TestContext testContext;

    public CreateSelectedDimensionStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("selected dimension's name is {string}")
    public void selectedDimensionSNameIs(final String name) {
        testContext.selectedDimension.setName(name);
    }

    @And("selected dimension's expression is {string}")
    public void selectedDimensionSExpressionIs(final String expression) {
        testContext.selectedDimension.setExpression(expression);
    }

    @And("selected dimension's isTimestamp is false")
    public void selectedDimensionSIsTimestampIsFalse() {
        testContext.selectedDimension.setIsTimestamp(false);
    }

    @And("selected dimension's isDate is false")
    public void selectedDimensionSIsDateIsFalse() {
        testContext.selectedDimension.setIsDate(false);
    }

    @And("selected dimensions's isString is false")
    public void selectedDimensionsSIsStringIsFalse() {
        testContext.selectedDimension.setIsString(false);
    }

    @And("selected dimension's visualizationType is {string}")
    public void selectedDimensionSVisualizationTypeIs(final String visualizationType) {
        testContext.selectedDimension.setVisualizationType(visualizationType);
    }

    @When("I create a selected dimension")
    public void iCreateASelectedDimension() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.selectedDimension, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "selected-dimensions");
        try {
            testContext.selectedDimensionId = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the selected dimension given above with response code {int} {string}")
    public void iShouldGetTheSelectedDimensionGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.selectedDimensionId))
                .body("name", equalTo(testContext.selectedDimension.getName()))
                .body("expression", equalTo(testContext.selectedDimension.getExpression()))
                .body("isTimestamp", equalTo(testContext.selectedDimension.getIsTimestamp()))
                .body("isDate", equalTo(testContext.selectedDimension.getIsDate()))
                .body("isString", equalTo(testContext.selectedDimension.getIsString()))
                .body("visualizationType", equalTo(testContext.selectedDimension.getVisualizationType()));
    }
}
