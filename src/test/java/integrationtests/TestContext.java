package integrationtests;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.dimension.Dimension;
import com.silensoft.conflated.configuration.service.filter.Filter;
import com.silensoft.conflated.configuration.service.griditem.GridItem;
import com.silensoft.conflated.configuration.service.measure.Measure;
import com.silensoft.conflated.configuration.service.selecteddimension.SelectedDimension;
import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilter;
import com.silensoft.conflated.configuration.service.selectedmeasure.SelectedMeasure;
import com.silensoft.conflated.configuration.service.selectedsortby.SelectedSortBy;
import com.silensoft.conflated.configuration.service.sortby.SortBy;

@SuppressWarnings({"PublicField", "InstanceVariableOfConcreteClass"})
public class TestContext {
    public io.restassured.response.Response response;

    public final Dimension dimension = new Dimension();
    public final DataSource dataSource = new DataSource();
    public final Chart chart = new Chart();
    public final DashboardGroup dashboardGroup = new DashboardGroup();
    public final Dashboard dashboard = new Dashboard();
    public final Filter filter = new Filter();
    public final GridItem gridItem = new GridItem();
    public final Measure measure = new Measure();
    public final SelectedDimension selectedDimension = new SelectedDimension();
    public final SelectedFilter selectedFilter = new SelectedFilter();
    public final SelectedMeasure selectedMeasure = new SelectedMeasure();
    public final SelectedSortBy selectedSortBy = new SelectedSortBy();
    public final SortBy sortBy = new SortBy();

    public Integer dashboardId;
    public Integer dataSourceId;
    public Integer dimensionId;
    public Integer chartId;
    public Integer dashboardGroupId;
    public Integer filterId;
    public Integer gridItemId;
    public Integer measureId;
    public Integer selectedDimensionId;
    public Integer selectedFilterId;
    public Integer selectedMeasureId;
    public Integer selectedSortById;
    public Integer sortById;
}
