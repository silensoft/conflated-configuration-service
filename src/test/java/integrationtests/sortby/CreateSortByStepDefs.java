package integrationtests.sortby;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSortByStepDefs {
    private final TestContext testContext;

    public CreateSortByStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @Given("sort by's name is {string}")
    public void sortBySNameIs(final String name) {
        testContext.sortBy.setName(name);
    }

    @And("sort by's expression is {string}")
    public void sortBySExpressionIs(final String expression) {
        testContext.sortBy.setExpression(expression);
    }

    @And("sort by's type is {string}")
    public void sortBySTypeIs(final String type) {
        testContext.sortBy.setType(type);
    }

    @When("I create a sort by")
    public void iCreateASortBy() {
        testContext.response = given().contentType(ContentType.JSON).body(testContext.sortBy, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "sort-bys");
        try {
            testContext.sortById = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch (final Exception exception) {
            // NOOP
        }
    }

    @Then("I should get the sort by given above with response code {int} {string}")
    public void iShouldGetTheSortByGivenAboveWithResponseCode(final int responseCode, @SuppressWarnings("unused") final String responseCodeName) {
        testContext.response.then()
                .assertThat()
                .statusCode(responseCode)
                .body("id", equalTo(testContext.sortById))
                .body("name", equalTo(testContext.sortBy.getName()))
                .body("expression", equalTo(testContext.sortBy.getExpression()))
                .body("type", equalTo(testContext.sortBy.getType()));
    }
}
