package integrationtests.sortby;

import integrationtests.ITConstants;
import integrationtests.TestContext;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;

import static io.restassured.RestAssured.given;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class CreateSortByForDataSourceStepDefs {
    private final TestContext testContext;

    public CreateSortByForDataSourceStepDefs(final TestContext newTestContext) {
        testContext = newTestContext;
    }

    @When("I create a new sort by for the given data source")
    public void iCreateANewSortByForTheGivenDataSource() {
        createSortByForDataSourceWithId(testContext.dataSourceId);
        try {
            testContext.sortById = testContext.response.then().contentType(ContentType.JSON).extract().path("id");
        } catch(final Exception exception) {
            // NOOP
        }
    }

    @When("I create a new sort by for data source with id {int}")
    public void iCreateANewSortByForDataSourceWithId(final int dataSourceId) {
        createSortByForDataSourceWithId(dataSourceId);
    }

    private void createSortByForDataSourceWithId(final int dataSourceId) {
        testContext.response = given()
                .pathParam("parentId", dataSourceId)
                .contentType(ContentType.JSON)
                .body(testContext.sortBy, ObjectMapperType.GSON).when()
                .post(ITConstants.BASE_URL + "data-sources/{parentId}/sort-bys");
    }
}
