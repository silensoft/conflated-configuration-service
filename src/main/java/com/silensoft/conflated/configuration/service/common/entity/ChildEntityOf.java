package com.silensoft.conflated.configuration.service.common.entity;

import java.util.List;

public interface ChildEntityOf<P> extends IdentifiableEntity {
    void setParentEntity(final P parentEntity);
    List<P> getParentEntities();
}
