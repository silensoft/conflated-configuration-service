package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface SelectedFilterStore extends EntityStore<SelectedFilter> {
}
