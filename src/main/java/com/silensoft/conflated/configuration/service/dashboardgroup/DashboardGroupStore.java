package com.silensoft.conflated.configuration.service.dashboardgroup;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface DashboardGroupStore extends EntityStore<DashboardGroup> {
}
