package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class SelectedFilterStoreImpl extends AbstractEntityStoreImpl<SelectedFilter> implements SelectedFilterStore {
    @Override
    protected Class<SelectedFilter> getEntityClass() {
        return SelectedFilter.class;
    }
}
