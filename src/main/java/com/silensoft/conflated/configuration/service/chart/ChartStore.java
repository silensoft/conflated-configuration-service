package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface ChartStore extends EntityStore<Chart> {
}
