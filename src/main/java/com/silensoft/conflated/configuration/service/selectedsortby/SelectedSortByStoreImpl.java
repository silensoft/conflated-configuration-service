package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class SelectedSortByStoreImpl extends AbstractEntityStoreImpl<SelectedSortBy> implements SelectedSortByStore {
    @Override
    protected Class<SelectedSortBy> getEntityClass() {
        return SelectedSortBy.class;
    }
}

