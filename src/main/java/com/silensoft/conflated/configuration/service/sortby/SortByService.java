package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("sort-bys")
public class SortByService extends AbstractCrudService<SortBy> {
    @Inject
    private SortByStore sortByStore;

    @Override
    protected EntityStore<SortBy> getEntityStore() {
        return sortByStore;
    }
}
