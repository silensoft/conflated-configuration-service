package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import com.silensoft.conflated.configuration.service.dashboard.DashboardStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dashboards/{parentId}/grid-items")
public class DashboardGridItemService extends AbstractChildCrudServiceForParent<Dashboard, GridItem> {
    @Inject
    private GridItemStore gridItemStore;

    @Inject
    private DashboardStore dashboardStore;

    @Override
    protected EntityStore<GridItem> getChildEntityStore() {
        return gridItemStore;
    }

    @Override
    protected EntityStore<Dashboard> getParentEntityStore() {
        return dashboardStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dashboard";
    }
}
