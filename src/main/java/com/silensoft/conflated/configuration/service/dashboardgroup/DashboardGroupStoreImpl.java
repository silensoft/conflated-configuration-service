package com.silensoft.conflated.configuration.service.dashboardgroup;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class DashboardGroupStoreImpl extends AbstractEntityStoreImpl<DashboardGroup> implements DashboardGroupStore {
    @Override
    protected Class<DashboardGroup> getEntityClass() {
        return DashboardGroup.class;
    }
}
