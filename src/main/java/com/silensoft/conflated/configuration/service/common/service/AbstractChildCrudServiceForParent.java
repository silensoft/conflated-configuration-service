package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@SuppressWarnings({"LawOfDemeter", "FeatureEnvy"})
public abstract class AbstractChildCrudServiceForParent<P extends ParentEntity<P>, C extends ChildEntityOf<P>> {

    protected abstract EntityStore<C> getChildEntityStore();

    protected abstract EntityStore<P> getParentEntityStore();

    protected abstract String getParentEntitiesName();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createChildEntityUnderParent(
            final @NotNull @Valid C newChildEntity,
            @Context final UriInfo uriInfo,
            @PathParam("parentId") final long parentId
    ) throws EntityNotFoundException {
        final P parentEntity = getParentEntityStore().readEntityWithId(parentId);
        parentEntity.addChildEntity(newChildEntity);
        getParentEntityStore().updateEntity(parentEntity);
        final ChildEntityOf<P> childEntityWithId = parentEntity.getChildEntities(newChildEntity).get(parentEntity.getChildEntities(newChildEntity).size() - 1);
        final URI createdEntityUri = uriInfo.getAbsolutePathBuilder().path("{id}").build(childEntityWithId.getId());
        return Response.created(createdEntityUri).entity(childEntityWithId).build();
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addChildEntityToParent(
            @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id,
            @PathParam("parentId") final long parentId
    ) throws EntityNotFoundException {
        final P parentEntity = getParentEntityStore().readEntityWithId(parentId);
        final C childEntity = getChildEntityStore().readEntityWithId(id);
        parentEntity.addChildEntity(childEntity);
        getParentEntityStore().updateEntity(parentEntity);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<C> readChildEntitiesOfParent(
            @DefaultValue(Constants.DEFAULT_NAME_FILTER) @QueryParam("nameFilter") final String nameFilter,
            @PathParam("parentId") final long parentId

    ) {
        return getChildEntityStore().readEntitiesForParent(getParentEntitiesName(), parentId);
    }

    @DELETE
    @Path("{id}")
    public void removeChildEntityFromParent(
            @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id,
            @PathParam("parentId") final long parentId
    ) throws EntityNotFoundException {
        final P parentEntity = getParentEntityStore().readEntityWithId(parentId);
        final C childEntity = getChildEntityStore().readEntityWithId(id);
        parentEntity.removeChildEntity(childEntity);
        getParentEntityStore().updateEntity(parentEntity);
    }
}
