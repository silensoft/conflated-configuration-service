package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.chart.AbstractChartChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Selectedmeasures", schema = "Conflated")
public class SelectedMeasure extends AbstractChartChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "expression is mandatory field") String expression;

    @Basic(optional = false)
    private @NotNull(message = "unit is mandatory field") String unit;

    @Basic(optional = false)
    private @NotNull(message = "aggregationFunction is mandatory field") String aggregationFunction;

    @Basic(optional = false)
    private @NotNull(message = "visualizationType is mandatory field") String visualizationType;

    @Basic(optional = false)
    private @NotNull(message = "visualizationColor is mandatory field") String visualizationColor;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(final String newUnit) {
        unit = newUnit;
    }

    public String getAggregationFunction() {
        return aggregationFunction;
    }

    public void setAggregationFunction(final String newAggregationFunction) {
        aggregationFunction = newAggregationFunction;
    }

    public String getVisualizationType() {
        return visualizationType;
    }

    public void setVisualizationType(final String newVisualizationType) {
        visualizationType = newVisualizationType;
    }

    public String getVisualizationColor() {
        return visualizationColor;
    }

    public void setVisualizationColor(final String newVisualizationColor) {
        visualizationColor = newVisualizationColor;
    }
}
