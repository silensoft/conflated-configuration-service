package com.silensoft.conflated.configuration.service.common.entity;

import java.util.List;

public interface ParentEntity<P> extends IdentifiableEntity {
    void addChildEntity(final ChildEntityOf<P> childEntity);
    void removeChildEntity(final ChildEntityOf<P> childEntity);
    List<ChildEntityOf<P>> getChildEntities(final ChildEntityOf<P> childEntity);
    void updateChildEntitiesFromOtherParentEntity(final ParentEntity<P> otherParentEntity);
    void removeAllChildEntities(final ChildEntityOf<P> childEntity);
    void removeAllChildEntities();
    boolean hasChildEntities();

    default void replaceChildEntity(final ChildEntityOf<P> oldChildEntity, final ChildEntityOf<P> newChildEntity) {
        this.removeChildEntity(oldChildEntity);
        this.addChildEntity(newChildEntity);
    }

    default void updateChildEntitiesFromOtherChildEntities(final List<ChildEntityOf<P>> otherChildEntities) {
        if (!otherChildEntities.isEmpty()) {
            this.removeAllChildEntities(otherChildEntities.get(0));

            for (final ChildEntityOf<P> otherChildEntity : otherChildEntities) {
                this.addChildEntity(otherChildEntity);
            }
        }
    }
}
