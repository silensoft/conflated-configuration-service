package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface SortByStore extends EntityStore<SortBy> {
}
