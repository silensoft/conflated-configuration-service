package com.silensoft.conflated.configuration.service.common.exceptions.entity;

public class DuplicateEntityException extends Exception {
    public DuplicateEntityException(final String message) {
        super(message);
    }
}
