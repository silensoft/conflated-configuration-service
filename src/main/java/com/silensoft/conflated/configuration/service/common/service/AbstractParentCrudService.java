package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@SuppressWarnings({"LawOfDemeter", "FeatureEnvy"})
public abstract class AbstractParentCrudService<E extends ParentEntity<E>> extends AbstractCrudService<E> {
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public void updateEntity(final @NotNull @Valid E updatedEntity,
                             @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id
    ) throws EntityNotFoundException {
        if (updatedEntity.hasChildEntities()) {
            final E currentEntity = getEntityStore().readAndDetachEntityWithId(id);
            currentEntity.updateChildEntitiesFromOtherParentEntity(updatedEntity);
            getEntityStore().updateEntity(currentEntity);
            updatedEntity.removeAllChildEntities();
        } else {
            getEntityStore().updateEntityWithId(id, updatedEntity);
        }
    }
}
