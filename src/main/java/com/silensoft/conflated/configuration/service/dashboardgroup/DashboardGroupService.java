package com.silensoft.conflated.configuration.service.dashboardgroup;

import com.silensoft.conflated.configuration.service.common.service.AbstractParentCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dashboard-groups")
public class DashboardGroupService extends AbstractParentCrudService<DashboardGroup> {
    @Inject
    private DashboardGroupStore dashboardGroupStore;

    @Override
    protected EntityStore<DashboardGroup> getEntityStore() {
        return dashboardGroupStore;
    }
}
