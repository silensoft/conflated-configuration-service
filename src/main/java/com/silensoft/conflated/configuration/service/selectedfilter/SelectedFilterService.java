package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("selected-filters")
public class SelectedFilterService extends AbstractCrudService<SelectedFilter> {
    @Inject
    private SelectedFilterStore selectedFilterStore;

    @Override
    protected EntityStore<SelectedFilter> getEntityStore() {
        return selectedFilterStore;
    }
}
