package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("selected-measures")
public class SelectedMeasureService extends AbstractCrudService<SelectedMeasure> {
    @Inject
    private SelectedMeasureStore selectedMeasureStore;

    @Override
    protected EntityStore<SelectedMeasure> getEntityStore() {
        return selectedMeasureStore;
    }
}
