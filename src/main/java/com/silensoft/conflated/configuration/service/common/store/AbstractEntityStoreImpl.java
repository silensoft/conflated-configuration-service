package com.silensoft.conflated.configuration.service.common.store;

import com.silensoft.conflated.configuration.service.common.entity.IdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.types.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public abstract class AbstractEntityStoreImpl<T extends IdentifiableEntity> implements EntityStore<T> {
    private static final String ENTITY_NOT_FOUND_WITH_ID = "Entity not found with id: ";

    @PersistenceContext(unitName = "default")
    private EntityManager entityManager;

    protected abstract Class<T> getEntityClass();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void createEntity(final T entity) throws DuplicateEntityException {
        try {
            getEntityManager().persist(entity);
            getEntityManager().flush();
        } catch (final EntityExistsException exception) {
            throw new DuplicateEntityException(exception.getMessage());
        }
    }

    @Override
    public List<T> readEntitiesForParent(final String parentEntitiesName, final long parentId) {
        final String queryString = "select e FROM " +
                getEntityClass().getName() +
                " e JOIN e." +
                parentEntitiesName +
                " p WHERE p.id = :parentId";
        final TypedQuery<T> query = getEntityManager().createQuery(queryString, getEntityClass());
        query.setParameter("parentId", parentId);
        return query.getResultList();
    }

    @Override
    public List<T> readEntities() {
        final CriteriaQuery<T> allEntitiesQuery = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
        allEntitiesQuery.select(allEntitiesQuery.from(getEntityClass()));
        return getEntityManager().createQuery(allEntitiesQuery).getResultList();
    }

    @Override
    public T readEntityWithId(final long id) throws EntityNotFoundException {
        //noinspection LawOfDemeter
        return Optional.ofNullable(getEntityManager().find(getEntityClass(), id))
                .orElseThrow(new EntityNotFoundException(ENTITY_NOT_FOUND_WITH_ID + id));
    }

    @Override
    public T readAndDetachEntityWithId(final long id) throws EntityNotFoundException {
        final T entity = readEntityWithId(id);
        getEntityManager().detach(entity);
        return entity;
    }

    @Override
    public void updateEntityWithId(final long id, final T updatedEntity) throws EntityNotFoundException {
        updatedEntity.setId(id);
        //noinspection LawOfDemeter
        Optional.ofNullable(getEntityManager().find(getEntityClass(), id))
                .ifPresentOrElseThrow(
                        entity -> getEntityManager().merge(updatedEntity),
                        new EntityNotFoundException(ENTITY_NOT_FOUND_WITH_ID + id)
                );
    }

    @SuppressWarnings({"FeatureEnvy", "LawOfDemeter"})
    @Override
    public T updateAndGetEntityWithId(final long id, final T updatedEntity) throws EntityNotFoundException {
        updatedEntity.setId(id);
        return Optional.ofNullable(getEntityManager().find(getEntityClass(), id))
                .map(entity -> Optional.ofNullable(getEntityManager().merge(updatedEntity)))
                .orElseThrow(new EntityNotFoundException(ENTITY_NOT_FOUND_WITH_ID + id));

    }

    public void updateEntity(final T updatedEntity) {
        getEntityManager().merge(updatedEntity);
        getEntityManager().flush();
    }

    @Override
    public void deleteAllEntities() {
        final CriteriaDelete<T> delete = getEntityManager().getCriteriaBuilder().createCriteriaDelete(getEntityClass());
        getEntityManager().createQuery(delete).executeUpdate();
    }

    @SuppressWarnings("LawOfDemeter")
    @Override
    public void deleteEntityWithId(final long id) throws EntityNotFoundException {
        getEntityManager().remove(Optional.ofNullable(getEntityManager().find(getEntityClass(), id))
                .orElseThrow(new EntityNotFoundException(ENTITY_NOT_FOUND_WITH_ID + id)));
    }

    @Override
    public void deleteEntity(final T entity) {
        getEntityManager().remove(entity);
    }
}
