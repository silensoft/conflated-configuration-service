package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface SelectedMeasureStore extends EntityStore<SelectedMeasure> {
}
