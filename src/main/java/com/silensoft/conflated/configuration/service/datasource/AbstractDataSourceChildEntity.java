package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Collections;
import java.util.List;

@MappedSuperclass
@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class AbstractDataSourceChildEntity extends AbstractIdentifiableEntity implements ChildEntityOf<DataSource> {
    @JsonbTransient
    @ManyToOne
    private DataSource dataSource;

    @Override
    public void setParentEntity(final DataSource newDataSource) {
        dataSource = newDataSource;
    }

    @JsonbTransient
    @Override
    public List<DataSource> getParentEntities() {
        return Collections.singletonList(dataSource);
    }
}

