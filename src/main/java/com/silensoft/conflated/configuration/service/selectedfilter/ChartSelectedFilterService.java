package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.chart.ChartStore;
import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("charts/{parentId}/selected-filters")
public class ChartSelectedFilterService extends AbstractChildCrudServiceForParent<Chart, SelectedFilter> {
    @Inject
    private SelectedFilterStore selectedDimensionStore;

    @Inject
    private ChartStore chartStore;

    @Override
    protected EntityStore<SelectedFilter> getChildEntityStore() {
        return selectedDimensionStore;
    }

    @Override
    protected EntityStore<Chart> getParentEntityStore() {
        return chartStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "chart";
    }
}
