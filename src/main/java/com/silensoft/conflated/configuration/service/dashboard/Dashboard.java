package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import com.silensoft.conflated.configuration.service.griditem.GridItem;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"AssignmentOrReturnOfFieldWithMutableType", "InstanceofConcreteClass", "ChainOfInstanceofChecks", "CastToConcreteClass", "unused", "MethodParameterOfConcreteClass"})
@Entity
@Table(name = "Dashboards", schema = "Conflated")
public class Dashboard extends AbstractIdentifiableEntity implements ParentEntity<Dashboard>, ChildEntityOf<DashboardGroup> {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "theme is mandatory field") String theme;

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dashboard", orphanRemoval = true)
    private List<GridItem> layout = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dashboard", orphanRemoval = true)
    private List<Chart> charts = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @JsonbTransient
    @SuppressWarnings("FieldMayBeFinal")
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Dashboard_dashboardgroup", schema = "Conflated",
            joinColumns = @JoinColumn(name = "dashboard_id"),
            inverseJoinColumns = @JoinColumn(name = "dashboardgroup_id")
    )
    private List<DashboardGroup> dashboardGroups = new ArrayList<>(Constants.INITIAL_CAPACITY);

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public List<GridItem> getLayout() {
        return layout;
    }

    public List<Chart> getCharts() {
        return charts;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(final String newTheme) {
        theme = newTheme;
    }

    @Override
    public void addChildEntity(final ChildEntityOf<Dashboard> childEntity) {
        childEntity.setParentEntity(this);

        if (childEntity instanceof Chart) {
            charts.add((Chart) childEntity);
        } else if (childEntity instanceof GridItem) {
            layout.add((GridItem) childEntity);
        }
    }

    @Override
    public void removeChildEntity(final ChildEntityOf<Dashboard> childEntity) {
        childEntity.setParentEntity(null);

        if (childEntity instanceof Chart) {
            charts.remove(childEntity);
        } else if (childEntity instanceof GridItem) {
            layout.remove(childEntity);
        }
    }

    @Override
    public List<ChildEntityOf<Dashboard>> getChildEntities(final ChildEntityOf<Dashboard> childEntity) {
        return childEntity instanceof Chart ? new ArrayList<>(getCharts()) : new ArrayList<>(getLayout());
    }

    @Override
    public void updateChildEntitiesFromOtherParentEntity(final ParentEntity<Dashboard> otherParentEntity) {
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new Chart()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new GridItem()));
    }

    @Override
    public void removeAllChildEntities(final ChildEntityOf<Dashboard> childEntity) {
        if (childEntity instanceof Chart) {
            charts.clear();
        } else if (childEntity instanceof GridItem) {
            layout.clear();
        }
    }

    @Override
    public void removeAllChildEntities() {
        charts.clear();
        layout.clear();
    }

    @Override
    public boolean hasChildEntities() {
        return !charts.isEmpty() || !layout.isEmpty();
    }

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public void setParentEntity(final DashboardGroup parentEntity) {
        dashboardGroups.add(parentEntity);
    }

    @JsonbTransient
    @Override
    public List<DashboardGroup> getParentEntities() {
        return dashboardGroups;
    }
}
