package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.datasource.AbstractDataSourceChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Measures", schema = "Conflated")
public class Measure extends AbstractDataSourceChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "expression is mandatory field") String expression;

    @Basic(optional = false)
    private @NotNull(message = "unit is mandatory field") String unit;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(final String newUnit) {
        unit = newUnit;
    }
}

