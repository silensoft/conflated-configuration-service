package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("data-sources/{parentId}/dimensions")
public class DataSourceDimensionService extends AbstractChildCrudServiceForParent<DataSource, Dimension> {
    @Inject
    private DimensionStore dimensionStore;

    @Inject
    private DataSourceStore dataSourceStore;

    @Override
    protected EntityStore<Dimension> getChildEntityStore() {
        return dimensionStore;
    }

    @Override
    protected EntityStore<DataSource> getParentEntityStore() {
        return dataSourceStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dataSource";
    }
}
