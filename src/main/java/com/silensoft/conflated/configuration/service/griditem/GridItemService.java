package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("grid-items")
public class GridItemService extends AbstractCrudService<GridItem> {
    @Inject
    private GridItemStore gridItemStore;

    @Override
    protected EntityStore<GridItem> getEntityStore() {
        return gridItemStore;
    }
}
