package com.silensoft.conflated.configuration.service.common.types;

@SuppressWarnings("MethodReturnOfConcreteClass")
public interface Mapper<T, U> {
    Optional<U> map(final T value);
}
