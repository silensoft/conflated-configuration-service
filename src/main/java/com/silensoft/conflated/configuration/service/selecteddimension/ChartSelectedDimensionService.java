package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.chart.ChartStore;
import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("charts/{parentId}/selected-dimensions")
public class ChartSelectedDimensionService extends AbstractChildCrudServiceForParent<Chart, SelectedDimension> {
    @Inject
    private SelectedDimensionStore selectedDimensionStore;

    @Inject
    private ChartStore chartStore;

    @Override
    protected EntityStore<SelectedDimension> getChildEntityStore() {
        return selectedDimensionStore;
    }

    @Override
    protected EntityStore<Chart> getParentEntityStore() {
        return chartStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "chart";
    }
}
