package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("data-sources/{parentId}/sort-bys")
public class DataSourceSortByService extends AbstractChildCrudServiceForParent<DataSource, SortBy> {
    @Inject
    private SortByStore sortByStore;

    @Inject
    private DataSourceStore dataSourceStore;

    @Override
    protected EntityStore<SortBy> getChildEntityStore() {
        return sortByStore;
    }

    @Override
    protected EntityStore<DataSource> getParentEntityStore() {
        return dataSourceStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dataSource";
    }
}
