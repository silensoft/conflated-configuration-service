package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class SelectedDimensionStoreImpl extends AbstractEntityStoreImpl<SelectedDimension> implements SelectedDimensionStore {
    @Override
    protected Class<SelectedDimension> getEntityClass() {
        return SelectedDimension.class;
    }
}
