package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dimensions")
public class DimensionService extends AbstractCrudService<Dimension> {
    @Inject
    private DimensionStore dimensionStore;

    @Override
    protected EntityStore<Dimension> getEntityStore() {
        return dimensionStore;
    }
}
