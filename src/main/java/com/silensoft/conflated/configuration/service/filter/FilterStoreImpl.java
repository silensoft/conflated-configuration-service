package com.silensoft.conflated.configuration.service.filter;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class FilterStoreImpl extends AbstractEntityStoreImpl<Filter> implements FilterStore {
    @Override
    protected Class<Filter> getEntityClass() {
        return Filter.class;
    }
}
