package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.IdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@SuppressWarnings("LawOfDemeter")
public abstract class AbstractCrudService<E extends IdentifiableEntity> {
    protected abstract EntityStore<E> getEntityStore();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEntity(final @NotNull @Valid E newEntity, @Context final UriInfo uriInfo) throws DuplicateEntityException {
        getEntityStore().createEntity(newEntity);
        final URI createdEntityUri = uriInfo.getAbsolutePathBuilder().path("{id}").build(newEntity.getId());
        return Response.created(createdEntityUri).entity(newEntity).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<E> readEntities(@DefaultValue(Constants.DEFAULT_NAME_FILTER) @QueryParam("nameFilter") final String nameFilter) {
        return getEntityStore().readEntities();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public E readEntity(@PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id) throws EntityNotFoundException {
        return getEntityStore().readEntityWithId(id);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateEntity(final @NotNull @Valid E updatedEntity,
                             @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id
    ) throws EntityNotFoundException {
        getEntityStore().updateEntityWithId(id, updatedEntity);
    }

    @DELETE
    public void deleteAllEntities(@DefaultValue(Constants.DEFAULT_NAME_FILTER) @QueryParam("nameFilter") final String nameFilter) {
        getEntityStore().deleteAllEntities();
    }

    @DELETE
    @Path("{id}")
    public void deleteEntity(@PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id) throws EntityNotFoundException {
        getEntityStore().deleteEntityWithId(id);
    }
}
