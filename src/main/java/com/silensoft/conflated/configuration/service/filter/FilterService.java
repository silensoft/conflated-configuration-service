package com.silensoft.conflated.configuration.service.filter;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("filters")
public class FilterService extends AbstractCrudService<Filter> {
    @Inject
    private FilterStore filterStore;

    @Override
    protected EntityStore<Filter> getEntityStore() {
        return filterStore;
    }
}
