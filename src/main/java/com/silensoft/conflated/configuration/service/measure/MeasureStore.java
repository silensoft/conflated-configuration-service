package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface MeasureStore extends EntityStore<Measure> {
}
