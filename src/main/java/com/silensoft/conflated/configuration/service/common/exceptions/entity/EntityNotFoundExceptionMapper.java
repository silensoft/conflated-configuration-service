package com.silensoft.conflated.configuration.service.common.exceptions.entity;

import javax.ejb.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
@Singleton
public class EntityNotFoundExceptionMapper implements ExceptionMapper<EntityNotFoundException> {
    private static final Logger logger = Logger.getLogger(EntityNotFoundExceptionMapper.class.getName());

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public Response toResponse(final EntityNotFoundException exception) {
        logger.log(Level.INFO, "DEBUG exception mapper");
        return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
    }
}
