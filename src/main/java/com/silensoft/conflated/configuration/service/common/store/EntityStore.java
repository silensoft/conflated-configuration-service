package com.silensoft.conflated.configuration.service.common.store;


import com.silensoft.conflated.configuration.service.common.entity.IdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;

import java.util.List;

public interface EntityStore<T extends IdentifiableEntity> {
    void createEntity(final T entity) throws DuplicateEntityException;
    List<T> readEntitiesForParent(final String parentEntitiesName, final long parentId);
    List<T> readEntities();
    T readEntityWithId(final long id) throws EntityNotFoundException;
    T readAndDetachEntityWithId(final long id) throws EntityNotFoundException;
    void updateEntityWithId(long id, final T updatedEntity) throws EntityNotFoundException;
    T updateAndGetEntityWithId(long id, final T updatedEntity) throws EntityNotFoundException;
    void updateEntity(final T newEntity);
    void deleteAllEntities();
    void deleteEntityWithId(final long id) throws EntityNotFoundException;
    void deleteEntity(final T entity);
}
