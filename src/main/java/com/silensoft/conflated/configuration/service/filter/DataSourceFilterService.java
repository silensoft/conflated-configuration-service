package com.silensoft.conflated.configuration.service.filter;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("data-sources/{parentId}/filters")
public class DataSourceFilterService extends AbstractChildCrudServiceForParent<DataSource, Filter> {
    @Inject
    private FilterStore filterStore;

    @Inject
    private DataSourceStore dataSourceStore;

    @Override
    protected EntityStore<Filter> getChildEntityStore() {
        return filterStore;
    }

    @Override
    protected EntityStore<DataSource> getParentEntityStore() {
        return dataSourceStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dataSource";
    }
}
