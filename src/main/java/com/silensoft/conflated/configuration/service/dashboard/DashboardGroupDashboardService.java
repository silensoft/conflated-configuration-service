package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dashboard-groups/{parentId}/dashboards")
public class DashboardGroupDashboardService extends AbstractChildCrudServiceForParent<DashboardGroup, Dashboard> {
    @Inject
    private DashboardStore dashboardStore;

    @Inject
    private DashboardGroupStore dashboardGroupStore;

    @Override
    protected EntityStore<DashboardGroup> getParentEntityStore() {
        return dashboardGroupStore;
    }

    @Override
    protected EntityStore<Dashboard> getChildEntityStore() {
        return dashboardStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dashboardGroups";
    }
}
