package com.silensoft.conflated.configuration.service.filter;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface FilterStore extends EntityStore<Filter> {
}
