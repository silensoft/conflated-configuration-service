package com.silensoft.conflated.configuration.service.common.entity;

public interface IdentifiableEntity {
    long getId();
    void setId(final long newId);
}
