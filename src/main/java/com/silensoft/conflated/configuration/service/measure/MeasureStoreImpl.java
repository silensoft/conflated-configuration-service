package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class MeasureStoreImpl extends AbstractEntityStoreImpl<Measure> implements MeasureStore {
    @Override
    protected Class<Measure> getEntityClass() {
        return Measure.class;
    }
}

