package com.silensoft.conflated.configuration.service;

import com.silensoft.conflated.configuration.service.chart.ChartService;
import com.silensoft.conflated.configuration.service.chart.DashboardChartService;
import com.silensoft.conflated.configuration.service.common.exceptions.ConstraintViolationExceptionMapper;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.DuplicateEntityExceptionMapper;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundExceptionMapper;
import com.silensoft.conflated.configuration.service.dashboard.DashboardGroupDashboardService;
import com.silensoft.conflated.configuration.service.dashboard.DashboardService;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupService;
import com.silensoft.conflated.configuration.service.datasource.DataSourceService;
import com.silensoft.conflated.configuration.service.dimension.DataSourceDimensionService;
import com.silensoft.conflated.configuration.service.dimension.DimensionService;
import com.silensoft.conflated.configuration.service.filter.DataSourceFilterService;
import com.silensoft.conflated.configuration.service.filter.FilterService;
import com.silensoft.conflated.configuration.service.griditem.DashboardGridItemService;
import com.silensoft.conflated.configuration.service.griditem.GridItemService;
import com.silensoft.conflated.configuration.service.measure.DataSourceMeasureService;
import com.silensoft.conflated.configuration.service.measure.MeasureService;
import com.silensoft.conflated.configuration.service.selecteddimension.ChartSelectedDimensionService;
import com.silensoft.conflated.configuration.service.selecteddimension.SelectedDimensionService;
import com.silensoft.conflated.configuration.service.selectedfilter.ChartSelectedFilterService;
import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilterService;
import com.silensoft.conflated.configuration.service.selectedmeasure.ChartSelectedMeasureService;
import com.silensoft.conflated.configuration.service.selectedmeasure.SelectedMeasureService;
import com.silensoft.conflated.configuration.service.selectedsortby.ChartSelectedSortByService;
import com.silensoft.conflated.configuration.service.selectedsortby.SelectedSortByService;
import com.silensoft.conflated.configuration.service.sortby.DataSourceSortByService;
import com.silensoft.conflated.configuration.service.sortby.SortByService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("v1")
public class ConflatedConfigurationServiceApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> resources = new HashSet<>(1);

        resources.add(ChartService.class);
        resources.add(DashboardChartService.class);
        resources.add(DataSourceService.class);
        resources.add(DataSourceMeasureService.class);
        resources.add(MeasureService.class);
        resources.add(DataSourceDimensionService.class);
        resources.add(DimensionService.class);
        resources.add(DataSourceFilterService.class);
        resources.add(FilterService.class);
        resources.add(DataSourceSortByService.class);
        resources.add(SortByService.class);
        resources.add(DashboardGroupService.class);
        resources.add(DashboardGroupDashboardService.class);
        resources.add(DashboardService.class);
        resources.add(DashboardGridItemService.class);
        resources.add(GridItemService.class);
        resources.add(ChartSelectedDimensionService.class);
        resources.add(SelectedDimensionService.class);
        resources.add(ChartSelectedFilterService.class);
        resources.add(SelectedFilterService.class);
        resources.add(ChartSelectedMeasureService.class);
        resources.add(SelectedMeasureService.class);
        resources.add(ChartSelectedSortByService.class);
        resources.add(SelectedSortByService.class);
        resources.add(ConstraintViolationExceptionMapper.class);
        resources.add(DuplicateEntityExceptionMapper.class);
        resources.add(EntityNotFoundExceptionMapper.class);

        return resources;
    }
}
