package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("selected-sort-bys")
public class SelectedSortByService extends AbstractCrudService<SelectedSortBy> {
    @Inject
    private SelectedSortByStore selectedSortByStore;

    @Override
    protected EntityStore<SelectedSortBy> getEntityStore() {
        return selectedSortByStore;
    }
}
