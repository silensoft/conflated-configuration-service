package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface DashboardStore extends EntityStore<Dashboard> {
}
