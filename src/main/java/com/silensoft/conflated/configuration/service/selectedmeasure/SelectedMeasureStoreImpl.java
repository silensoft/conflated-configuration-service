package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class SelectedMeasureStoreImpl extends AbstractEntityStoreImpl<SelectedMeasure> implements SelectedMeasureStore {
    @Override
    protected Class<SelectedMeasure> getEntityClass() {
        return SelectedMeasure.class;
    }
}
