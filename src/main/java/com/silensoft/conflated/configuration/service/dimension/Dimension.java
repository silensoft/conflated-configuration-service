package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.datasource.AbstractDataSourceChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Dimensions", schema = "Conflated")
public class Dimension extends AbstractDataSourceChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "expression is mandatory field") String expression;

    @Basic(optional = false)
    private @NotNull(message = "isTimestamp is mandatory field") Boolean isTimestamp;

    @Basic(optional = false)
    private @NotNull(message = "isDate is mandatory field") Boolean isDate;

    @Basic(optional = false)
    private @NotNull(message = "isString is mandatory field") Boolean isString;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public boolean getIsTimestamp() {
        return isTimestamp;
    }

    public void setIsTimestamp(final Boolean newIsTimestamp) {
        isTimestamp = newIsTimestamp;
    }

    public boolean getIsDate() {
        return isDate;
    }

    public void setIsDate(final Boolean newIsDate) {
        isDate = newIsDate;
    }

    public boolean getIsString() {
        return isString;
    }

    public void setIsString(final Boolean newIsString) {
        isString = newIsString;
    }
}