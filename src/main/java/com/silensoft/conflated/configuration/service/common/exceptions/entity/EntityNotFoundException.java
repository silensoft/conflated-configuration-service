package com.silensoft.conflated.configuration.service.common.exceptions.entity;

public class EntityNotFoundException extends Exception {
    public EntityNotFoundException(final String message) {
        super(message);
    }
}
