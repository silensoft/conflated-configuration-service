package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.chart.AbstractChartChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@SuppressWarnings("unused")
@Entity
@Table(name = "Selectedsortbys", schema = "Conflated")
public class SelectedSortBy extends AbstractChartChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "measureOrDimensionName is mandatory field") String measureOrDimensionName;

    @Basic(optional = false)
    private @NotNull(message = "measureOrDimensionExpression is mandatory field") String measureOrDimensionExpression;

    @Basic(optional = false)
    private @NotNull(message = "type is mandatory field") String type;

    @Basic(optional = false)
    private @NotNull(message = "aggregationFunction is mandatory field") String aggregationFunction;

    @Basic(optional = false)
    private @NotNull(message = "timeSortOption is mandatory field") String timeSortOption;

    @Basic(optional = false)
    private @NotNull(message = "sortDirection is mandatory field") String sortDirection;

    @Basic(optional = false)
    private @NotNull(message = "sortByDataScopeType is mandatory field") String sortByDataScopeType;

    @Basic(optional = false)
    private @NotNull(message = "defaultSortByType is mandatory field") String defaultSortByType;

    public String getMeasureOrDimensionName() {
        return measureOrDimensionName;
    }

    public void setMeasureOrDimensionName(final String newMeasureOrDimensionName) {
        measureOrDimensionName = newMeasureOrDimensionName;
    }

    public String getMeasureOrDimensionExpression() {
        return measureOrDimensionExpression;
    }

    public void setMeasureOrDimensionExpression(final String newMeasureOrDimensionExpression) {
        measureOrDimensionExpression = newMeasureOrDimensionExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }

    public String getAggregationFunction() {
        return aggregationFunction;
    }

    public void setAggregationFunction(final String newAggregationFunction) {
        aggregationFunction = newAggregationFunction;
    }

    public String getTimeSortOption() {
        return timeSortOption;
    }

    public void setTimeSortOption(final String newTimeSortOption) {
        timeSortOption = newTimeSortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(final String newSortDirection) {
        sortDirection = newSortDirection;
    }

    public String getSortByDataScopeType() {
        return sortByDataScopeType;
    }

    public void setSortByDataScopeType(final String newSortByDataScopeType) {
        sortByDataScopeType = newSortByDataScopeType;
    }

    public String getDefaultSortByType() {
        return defaultSortByType;
    }

    public void setDefaultSortByType(final String newDefaultSortByType) {
        defaultSortByType = newDefaultSortByType;
    }
}

