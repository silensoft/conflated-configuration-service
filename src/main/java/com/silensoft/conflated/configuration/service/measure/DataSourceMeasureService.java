package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.datasource.DataSourceStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("data-sources/{parentId}/measures")
public class DataSourceMeasureService extends AbstractChildCrudServiceForParent<DataSource, Measure> {
    @Inject
    private MeasureStore measureStore;

    @Inject
    private DataSourceStore dataSourceStore;

    @Override
    protected EntityStore<Measure> getChildEntityStore() {
        return measureStore;
    }

    @Override
    protected EntityStore<DataSource> getParentEntityStore() {
        return dataSourceStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "dataSource";
    }
}
