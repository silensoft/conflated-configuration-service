package com.silensoft.conflated.configuration.service.dashboardgroup;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"AssignmentOrReturnOfFieldWithMutableType", "CastToConcreteClass", "LocalVariableOfConcreteClass", "unused", "MethodParameterOfConcreteClass"})
@Entity
@Table(name = "Dashboardgroups", schema = "Conflated")
public class DashboardGroup extends AbstractIdentifiableEntity implements ParentEntity<DashboardGroup> {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @SuppressWarnings("FieldMayBeFinal")
    @ManyToMany(mappedBy = "dashboardGroups", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Dashboard> dashboards = new ArrayList<>(Constants.INITIAL_CAPACITY);

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public List<Dashboard> getDashboards() {
        return dashboards;
    }

    public void setDashboards(final List<Dashboard> newDashboards) {
        dashboards = newDashboards;
    }

    public List<ChildEntityOf<DashboardGroup>> getChildEntities(final ChildEntityOf<DashboardGroup> childEntity) {
        return new ArrayList<>(getDashboards());
    }

    @Override
    public void updateChildEntitiesFromOtherParentEntity(final ParentEntity<DashboardGroup> otherParentEntity) {
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new Dashboard()));
    }

    public void addChildEntity(final ChildEntityOf<DashboardGroup> childEntity) {
        final Dashboard dashboard = (Dashboard) childEntity;
        dashboards.add(dashboard);
        final List<DashboardGroup> dashboardGroups = dashboard.getParentEntities();
        dashboardGroups.add(this);
    }

    public void removeChildEntity(final ChildEntityOf<DashboardGroup> childEntity) {
        final Dashboard dashboard = (Dashboard) childEntity;
        dashboards.remove(dashboard);
        dashboard.getParentEntities().remove(this);
    }

    public void removeAllChildEntities(final ChildEntityOf<DashboardGroup> childEntity) {
        dashboards.clear();
    }

    @Override
    public void removeAllChildEntities() {
        dashboards.clear();
    }

    public boolean hasChildEntities() {
        return !dashboards.isEmpty();
    }
}
