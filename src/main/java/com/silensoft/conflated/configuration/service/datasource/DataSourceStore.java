package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface DataSourceStore extends EntityStore<DataSource> {
}
