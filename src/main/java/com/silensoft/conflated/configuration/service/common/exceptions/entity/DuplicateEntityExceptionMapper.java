package com.silensoft.conflated.configuration.service.common.exceptions.entity;

import javax.ejb.Singleton;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class DuplicateEntityExceptionMapper implements ExceptionMapper<DuplicateEntityException> {

    @SuppressWarnings("MethodParameterOfConcreteClass")
    @Override
    public Response toResponse(final DuplicateEntityException exception) {
        return Response.status(Response.Status.CONFLICT).entity(exception.getMessage()).build();
    }
}
