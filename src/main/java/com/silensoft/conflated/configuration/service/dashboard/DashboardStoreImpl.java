package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class DashboardStoreImpl extends AbstractEntityStoreImpl<Dashboard> implements DashboardStore {
    @Override
    protected Class<Dashboard> getEntityClass() {
        return Dashboard.class;
    }
}
