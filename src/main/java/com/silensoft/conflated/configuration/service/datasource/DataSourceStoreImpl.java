package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class DataSourceStoreImpl extends AbstractEntityStoreImpl<DataSource> implements DataSourceStore  {
    @Override
    protected Class<DataSource> getEntityClass() {
        return DataSource.class;
    }
}
