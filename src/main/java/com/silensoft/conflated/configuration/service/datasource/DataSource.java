package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.dimension.Dimension;
import com.silensoft.conflated.configuration.service.filter.Filter;
import com.silensoft.conflated.configuration.service.measure.Measure;
import com.silensoft.conflated.configuration.service.sortby.SortBy;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"AssignmentOrReturnOfFieldWithMutableType", "ClassWithTooManyFields", "ClassWithTooManyMethods", "InstanceofConcreteClass", "CastToConcreteClass", "ChainOfInstanceofChecks", "unused", "MethodParameterOfConcreteClass"})
@Entity
@Table(name = "Datasources", schema = "Conflated")
public class DataSource extends AbstractIdentifiableEntity implements ParentEntity<DataSource> {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "jdbcDriverClass is mandatory field") String jdbcDriverClass;

    @Basic(optional = false)
    private @NotNull(message = "jdbcUrl is mandatory field") String jdbcUrl;

    @Basic(optional = false)
    private @NotNull(message = "userName is mandatory field") String userName;

    @Basic(optional = false)
    private @NotNull(message = "password is mandatory field") String password;

    @Basic(optional = false)
    private @NotNull(message = "sqlStatement is mandatory field") String sqlStatement;

    @Basic(optional = false)
    private @NotNull(message = "dataRefreshIntervalValue is mandatory field") Long dataRefreshIntervalValue;

    @Basic(optional = false)
    private @NotNull(message = "dataRefreshIntervalUnit is mandatory field") String dataRefreshIntervalUnit;

    @Basic(optional = false)
    private @NotNull(message = "dataRefreshDelayValue is mandatory field") Long dataRefreshDelayValue;

    @Basic(optional = false)
    private @NotNull(message = "dataRefreshDelayUnit is mandatory field") String dataRefreshDelayUnit;

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dataSource", orphanRemoval = true)
    private List<Measure> measures = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dataSource", orphanRemoval = true)
    private List<Dimension> dimensions = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dataSource", orphanRemoval = true)
    private List<Filter> filters = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "dataSource", orphanRemoval = true)
    private List<SortBy> sortBys = new ArrayList<>(Constants.INITIAL_CAPACITY);

    // TODO Hash password https://www.stubbornjava.com/posts/hashing-passwords-in-java-with-bcrypt
    // https://thoughts-on-java.org/how-to-use-jpa-type-converter-to/

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getJdbcDriverClass() {
        return jdbcDriverClass;
    }

    public void setJdbcDriverClass(final String newJdbcDriverClass) {
        jdbcDriverClass = newJdbcDriverClass;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(final String newJdbcUrl) {
        jdbcUrl = newJdbcUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String newUserName) {
        userName = newUserName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String newPassword) {
        password = newPassword;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setSqlStatement(final String newSqlStatement) {
        sqlStatement = newSqlStatement;
    }

    public long getDataRefreshIntervalValue() {
        return dataRefreshIntervalValue;
    }

    public void setDataRefreshIntervalValue(final Long newDataRefreshIntervalValue) {
        dataRefreshIntervalValue = newDataRefreshIntervalValue;
    }

    public String getDataRefreshIntervalUnit() {
        return dataRefreshIntervalUnit;
    }

    public void setDataRefreshIntervalUnit(final String newDataRefreshIntervalUnit) {
        dataRefreshIntervalUnit = newDataRefreshIntervalUnit;
    }

    public long getDataRefreshDelayValue() {
        return dataRefreshDelayValue;
    }

    public void setDataRefreshDelayValue(final Long newDataRefreshDelayValue) {
        this.dataRefreshDelayValue = newDataRefreshDelayValue;
    }

    public String getDataRefreshDelayUnit() {
        return dataRefreshDelayUnit;
    }

    public void setDataRefreshDelayUnit(final String newDataRefreshDelayUnit) {
        dataRefreshDelayUnit = newDataRefreshDelayUnit;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public List<Dimension> getDimensions() {
        return dimensions;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public List<SortBy> getSortBys() {
        return sortBys;
    }

    @Override
    public void addChildEntity(final ChildEntityOf<DataSource> childEntity) {
        childEntity.setParentEntity(this);

        if (childEntity instanceof Measure) {
            measures.add((Measure) childEntity);
        } else if (childEntity instanceof Dimension) {
            dimensions.add((Dimension) childEntity);
        } else if (childEntity instanceof Filter) {
            filters.add((Filter) childEntity);
        } else if (childEntity instanceof SortBy) {
            sortBys.add((SortBy) childEntity);
        }
    }

    @Override
    public void removeChildEntity(final ChildEntityOf<DataSource> childEntity) {
        childEntity.setParentEntity(null);

        if (childEntity instanceof Measure) {
            measures.remove(childEntity);
        } else if (childEntity instanceof Dimension) {
            dimensions.remove(childEntity);
        } else if (childEntity instanceof Filter) {
            filters.remove(childEntity);
        } else if (childEntity instanceof SortBy) {
            sortBys.remove(childEntity);
        }
    }

    @Override
    public List<ChildEntityOf<DataSource>> getChildEntities(final ChildEntityOf<DataSource> childEntity) {
        if (childEntity instanceof Measure) {
            return new ArrayList<>(getMeasures());
        } else if (childEntity instanceof Dimension) {
            return new ArrayList<>(getDimensions());
        } else if (childEntity instanceof Filter) {
            return new ArrayList<>(getFilters());
        } else {
            // Must be SortBy
            return new ArrayList<>(getSortBys());
        }
    }

    @Override
    public void updateChildEntitiesFromOtherParentEntity(final ParentEntity<DataSource> otherParentEntity) {
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new Measure()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new Dimension()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new Filter()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new SortBy()));
    }

    @Override
    public void removeAllChildEntities(final ChildEntityOf<DataSource> childEntity) {
        if (childEntity instanceof Measure) {
            measures.clear();
        } else if (childEntity instanceof Dimension) {
            dimensions.clear();
        } else if (childEntity instanceof Filter) {
            filters.clear();
        } else if (childEntity instanceof SortBy) {
            sortBys.clear();
        }
    }

    @Override
    public void removeAllChildEntities() {
        measures.clear();
        dimensions.clear();
        filters.clear();
        sortBys.clear();
    }

    @SuppressWarnings("MethodWithMoreThanThreeNegations")
    @Override
    public boolean hasChildEntities() {
        return !measures.isEmpty() || !dimensions.isEmpty() || !filters.isEmpty() || !sortBys.isEmpty();
    }
}
