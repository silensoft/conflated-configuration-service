package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface SelectedDimensionStore extends EntityStore<SelectedDimension> {
}
