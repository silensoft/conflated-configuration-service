package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface GridItemStore extends EntityStore<GridItem> {
}
