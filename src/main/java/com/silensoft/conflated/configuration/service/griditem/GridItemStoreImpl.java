package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class GridItemStoreImpl extends AbstractEntityStoreImpl<GridItem> implements GridItemStore {
    @Override
    protected Class<GridItem> getEntityClass() {
        return GridItem.class;
    }
}
