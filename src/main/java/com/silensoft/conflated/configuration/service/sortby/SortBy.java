package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.datasource.AbstractDataSourceChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Sortbys", schema = "Conflated")
public class SortBy extends AbstractDataSourceChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "name is mandatory field") String name;

    @Basic(optional = false)
    private @NotNull(message = "expression is mandatory field") String expression;

    @Basic(optional = false)
    private @NotNull(message = "type is mandatory field") String type;

    public String getName() {
        return name;
    }

    public void setName(final String newName) {
        name = newName;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(final String newExpression) {
        expression = newExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }
}
