package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboard.Dashboard;
import com.silensoft.conflated.configuration.service.dashboard.DashboardStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dashboards/{parentId}/charts")
public class DashboardChartService extends AbstractChildCrudServiceForParent<Dashboard, Chart> {
    @Inject
    private ChartStore chartStore;

    @Inject
    private DashboardStore dashboardStore;

    @Override
    public EntityStore<Chart> getChildEntityStore() {
        return chartStore;
    }

    @Override
    public EntityStore<Dashboard> getParentEntityStore() {
        return dashboardStore;
    }

    @Override
    public String getParentEntitiesName() {
        return "dashboard";
    }
}
