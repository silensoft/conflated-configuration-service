package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class ChartStoreImpl extends AbstractEntityStoreImpl<Chart> implements ChartStore {
    @Override
    public Class<Chart> getEntityClass() { return Chart.class; }
}
