package com.silensoft.conflated.configuration.service.selectedfilter;

import com.silensoft.conflated.configuration.service.chart.AbstractChartChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Selectedfilters", schema = "Conflated")
public class SelectedFilter extends AbstractChartChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "measureOrDimensionName is mandatory field") String measureOrDimensionName;

    @Basic(optional = false)
    private @NotNull(message = "measureOrDimensionExpression is mandatory field") String measureOrDimensionExpression;

    @Basic(optional = false)
    private @NotNull(message = "type is mandatory field") String type;

    @Basic(optional = false)
    private @NotNull(message = "aggregationFunction is mandatory field") String aggregationFunction;

    @Basic(optional = false)
    private @NotNull(message = "filterExpression is mandatory field") String filterExpression;

    @Basic(optional = false)
    private @NotNull(message = "filterInputType is mandatory field") String filterInputType;

    @Basic(optional = false)
    private @NotNull(message = "filterDataScopeType is mandatory field") String filterDataScopeType;

    @Basic(optional = false)
    private @NotNull(message = "allowedDimensionFilterInputTypes is mandatory field") String allowedDimensionFilterInputTypes;

    public String getMeasureOrDimensionName() {
        return measureOrDimensionName;
    }

    public void setMeasureOrDimensionName(final String newMeasureOrDimensionName) {
        measureOrDimensionName = newMeasureOrDimensionName;
    }

    public String getMeasureOrDimensionExpression() {
        return measureOrDimensionExpression;
    }

    public void setMeasureOrDimensionExpression(final String newMeasureOrDimensionExpression) {
        measureOrDimensionExpression = newMeasureOrDimensionExpression;
    }

    public String getType() {
        return type;
    }

    public void setType(final String newType) {
        type = newType;
    }

    public String getAggregationFunction() {
        return aggregationFunction;
    }

    public void setAggregationFunction(final String newAggregationFunction) {
        aggregationFunction = newAggregationFunction;
    }

    public String getFilterExpression() {
        return filterExpression;
    }

    public void setFilterExpression(final String newFilterExpression) {
        filterExpression = newFilterExpression;
    }

    public String getFilterInputType() {
        return filterInputType;
    }

    public void setFilterInputType(final String newFilterInputType) {
        filterInputType = newFilterInputType;
    }

    public String getFilterDataScopeType() {
        return filterDataScopeType;
    }

    public void setFilterDataScopeType(final String newFilterDataScopeType) {
        filterDataScopeType = newFilterDataScopeType;
    }

    public String getAllowedDimensionFilterInputTypes() {
        return allowedDimensionFilterInputTypes;
    }

    public void setAllowedDimensionFilterInputTypes(final String newAllowedDimensionFilterInputTypes) {
        allowedDimensionFilterInputTypes = newAllowedDimensionFilterInputTypes;
    }
}
