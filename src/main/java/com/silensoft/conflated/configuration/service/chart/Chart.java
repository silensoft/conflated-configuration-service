package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.dashboard.AbstractDashboardChildEntity;
import com.silensoft.conflated.configuration.service.datasource.DataSource;
import com.silensoft.conflated.configuration.service.selecteddimension.SelectedDimension;
import com.silensoft.conflated.configuration.service.selectedfilter.SelectedFilter;
import com.silensoft.conflated.configuration.service.selectedmeasure.SelectedMeasure;
import com.silensoft.conflated.configuration.service.selectedsortby.SelectedSortBy;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"InstanceVariableOfConcreteClass", "AssignmentOrReturnOfFieldWithMutableType", "InstanceofConcreteClass", "CastToConcreteClass", "ChainOfInstanceofChecks", "unused", "MethodReturnOfConcreteClass", "ClassWithTooManyMethods", "MethodParameterOfConcreteClass"})
@Entity
@Table(name = "Charts", schema = "Conflated")
public class Chart extends AbstractDashboardChildEntity implements ParentEntity<Chart> {
    @Basic(optional = false)
    private @NotNull(message = "layoutId is mandatory field") String layoutId;

    @Basic(optional = false)
    private @NotNull(message = "chartType is mandatory field") String chartType;

    @Basic(optional = false)
    private @NotNull(message = "xAxisCategoriesShownCount is mandatory field") Integer xAxisCategoriesShownCount;

    @Basic(optional = false)
    private @NotNull(message = "fetchedRowCount is mandatory field") Integer fetchedRowCount;

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "chart", orphanRemoval = true)
    private List<SelectedMeasure> selectedMeasures = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "chart", orphanRemoval = true)
    private List<SelectedDimension> selectedDimensions = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "chart", orphanRemoval = true)
    private List<SelectedFilter> selectedFilters = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @SuppressWarnings("FieldMayBeFinal")
    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "chart", orphanRemoval = true)
    private List<SelectedSortBy> selectedSortBys = new ArrayList<>(Constants.INITIAL_CAPACITY);

    @ManyToOne
    @JoinColumn(name = "datasource_id")
    private DataSource dataSource;

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(final String newLayoutId) {
        layoutId = newLayoutId;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(final DataSource newDataSource) {
        dataSource = newDataSource;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(final String newChartType) {
        chartType = newChartType;
    }

    public int getxAxisCategoriesShownCount() {
        return xAxisCategoriesShownCount;
    }

    public void setxAxisCategoriesShownCount(final Integer newXAxisCategoriesShownCount) {
        xAxisCategoriesShownCount = newXAxisCategoriesShownCount;
    }

    public int getFetchedRowCount() {
        return fetchedRowCount;
    }

    public void setFetchedRowCount(final Integer newFetchedRowCount) {
        fetchedRowCount = newFetchedRowCount;
    }

    public List<SelectedMeasure> getSelectedMeasures() {
        return selectedMeasures;
    }

    public List<SelectedDimension> getSelectedDimensions() {
        return selectedDimensions;
    }

    public List<SelectedFilter> getSelectedFilters() {
        return selectedFilters;
    }

    public List<SelectedSortBy> getSelectedSortBys() {
        return selectedSortBys;
    }

    @Override
    public void addChildEntity(final ChildEntityOf<Chart> childEntity) {
        childEntity.setParentEntity(this);

        if (childEntity instanceof SelectedMeasure) {
            selectedMeasures.add((SelectedMeasure) childEntity);
        } else if (childEntity instanceof SelectedDimension) {
            selectedDimensions.add((SelectedDimension) childEntity);
        } else if (childEntity instanceof SelectedFilter) {
            selectedFilters.add((SelectedFilter) childEntity);
        } else if (childEntity instanceof SelectedSortBy) {
            selectedSortBys.add((SelectedSortBy) childEntity);
        }
    }

    @Override
    public void removeChildEntity(final ChildEntityOf<Chart> childEntity) {
        childEntity.setParentEntity(null);

        if (childEntity instanceof SelectedMeasure) {
            selectedMeasures.remove(childEntity);
        } else if (childEntity instanceof SelectedDimension) {
            selectedDimensions.remove(childEntity);
        } else if (childEntity instanceof SelectedFilter) {
            selectedFilters.remove(childEntity);
        } else if (childEntity instanceof SelectedSortBy) {
            selectedSortBys.remove(childEntity);
        }
    }

    @Override
    public List<ChildEntityOf<Chart>> getChildEntities(final ChildEntityOf<Chart> childEntity) {
        if (childEntity instanceof SelectedMeasure) {
            return new ArrayList<>(getSelectedMeasures());
        } else if (childEntity instanceof SelectedDimension) {
            return new ArrayList<>(getSelectedDimensions());
        } else if (childEntity instanceof SelectedFilter) {
            return new ArrayList<>(getSelectedFilters());
        } else {
            // Must be SelectedSortBy
            return new ArrayList<>(getSelectedSortBys());
        }
    }

    @Override
    public void updateChildEntitiesFromOtherParentEntity(final ParentEntity<Chart> otherParentEntity) {
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new SelectedMeasure()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new SelectedDimension()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new SelectedFilter()));
        updateChildEntitiesFromOtherChildEntities(otherParentEntity.getChildEntities(new SelectedSortBy()));
    }

    @Override
    public void removeAllChildEntities(final ChildEntityOf<Chart> childEntity) {
        if (childEntity instanceof SelectedMeasure) {
            selectedMeasures.clear();
        } else if (childEntity instanceof SelectedDimension) {
            selectedDimensions.clear();
        } else if (childEntity instanceof SelectedFilter) {
            selectedFilters.clear();
        } else {
            // Must be SelectedSortBy
            selectedSortBys.clear();
        }
    }

    @Override
    public void removeAllChildEntities() {
        selectedMeasures.clear();
        selectedDimensions.clear();
        selectedFilters.clear();
        selectedSortBys.clear();
    }

    @SuppressWarnings("MethodWithMoreThanThreeNegations")
    @Override
    public boolean hasChildEntities() {
        return !selectedMeasures.isEmpty() || !selectedDimensions.isEmpty() || !selectedFilters.isEmpty() || !selectedSortBys.isEmpty();
    }
}