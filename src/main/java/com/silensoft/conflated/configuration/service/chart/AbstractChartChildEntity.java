package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Collections;
import java.util.List;

@MappedSuperclass
@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class AbstractChartChildEntity extends AbstractIdentifiableEntity implements ChildEntityOf<Chart> {
    @JsonbTransient
    @ManyToOne
    private Chart chart;

    @Override
    public void setParentEntity(final Chart newChart) {
        chart = newChart;
    }

    @JsonbTransient
    @Override
    public List<Chart> getParentEntities() {
        return Collections.singletonList(chart);
    }
}
