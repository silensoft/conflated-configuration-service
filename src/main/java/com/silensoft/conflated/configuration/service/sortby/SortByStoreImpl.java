package com.silensoft.conflated.configuration.service.sortby;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class SortByStoreImpl extends AbstractEntityStoreImpl<SortBy> implements SortByStore {
    @Override
    protected Class<SortBy> getEntityClass() {
        return SortBy.class;
    }
}
