package com.silensoft.conflated.configuration.service.common.types;

@SuppressWarnings("MethodReturnOfConcreteClass")
public final class Optional<T> {
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private final java.util.Optional<T> optional;

    public static <T> Optional<T> empty() {
        return new Optional<>();
    }

    public static <T> Optional<T> of(final T value) {
        return new Optional<>(value);
    }

    public static <T> Optional<T> ofNullable(final T value) {
        return new Optional<>(value);
    }

    private Optional() {
        optional = java.util.Optional.empty();
    }

    private Optional(final T value) {
        optional = java.util.Optional.ofNullable(value);
    }

    public <X extends Throwable> void ifPresentOrElseThrow(final Consumer<T> consumer, final X exception) throws X {
        if (optional.isPresent()) {
            consumer.consume(optional.get());
        } else {
            throw exception;
        }
    }

    public <U> Optional<U> map(final Mapper<T, U> mapper) {
        return optional.isPresent() ? mapper.map(optional.get()) : empty();
    }

    public <X extends Throwable> T orElseThrow(final X exception) throws X {
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw exception;
        }
    }
}