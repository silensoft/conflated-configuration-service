package com.silensoft.conflated.configuration.service.common.constants;

public final class Constants {
    public static final int INITIAL_CAPACITY = 20;
    public static final String DEFAULT_NAME_FILTER = "";
    public static final String ID_MUST_BE_GREATER_THAN_ZERO = "id must be greater than zero";

    private Constants() {
    }
}
