package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.common.store.AbstractEntityStoreImpl;

import javax.ejb.Stateless;

@Stateless
public class DimensionStoreImpl extends AbstractEntityStoreImpl<Dimension> implements DimensionStore {
    @Override
    protected Class<Dimension> getEntityClass() {
        return Dimension.class;
    }
}

