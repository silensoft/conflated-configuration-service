package com.silensoft.conflated.configuration.service.common.types;

public interface Consumer<T> {
    void consume(final T value);
}
