package com.silensoft.conflated.configuration.service.griditem;

import com.silensoft.conflated.configuration.service.dashboard.AbstractDashboardChildEntity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("unused")
@Entity
@Table(name = "Griditems", schema = "Conflated")
public class GridItem extends AbstractDashboardChildEntity {
    @Basic(optional = false)
    private @NotNull(message = "i is mandatory field") String i;

    @Basic(optional = false)
    private @NotNull(message = "x is mandatory field") Integer x;

    @Basic(optional = false)
    private @NotNull(message = "y is mandatory field") Integer y;

    @Basic(optional = false)
    private @NotNull(message = "w is mandatory field") Integer w;

    @Basic(optional = false)
    private @NotNull(message = "h is mandatory field") Integer h;

    public String getI() {
        return i;
    }

    public void setI(final String newI) {
        i = newI;
    }

    public int getX() {
        return x;
    }

    public void setX(final Integer newX) {
        x = newX;
    }

    public int getY() {
        return y;
    }

    public void setY(final Integer newY) {
        y = newY;
    }

    public int getW() {
        return w;
    }

    public void setW(final Integer newW) {
        w = newW;
    }

    public int getH() {
        return h;
    }

    public void setH(final Integer newH) {
        h = newH;
    }
}
