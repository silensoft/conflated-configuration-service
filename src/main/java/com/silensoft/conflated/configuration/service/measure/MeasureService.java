package com.silensoft.conflated.configuration.service.measure;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("measures")
public class MeasureService extends AbstractCrudService<Measure> {
    @Inject
    private MeasureStore measureStore;

    @Override
    protected EntityStore<Measure> getEntityStore() {
        return measureStore;
    }
}
