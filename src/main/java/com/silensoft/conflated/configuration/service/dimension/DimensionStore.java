package com.silensoft.conflated.configuration.service.dimension;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface DimensionStore extends EntityStore<Dimension> {
}
