package com.silensoft.conflated.configuration.service.common.service;

import com.silensoft.conflated.configuration.service.common.constants.Constants;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;
import com.silensoft.conflated.configuration.service.common.entity.ParentEntity;
import com.silensoft.conflated.configuration.service.common.exceptions.entity.EntityNotFoundException;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"LawOfDemeter", "FeatureEnvy"})
public abstract class AbstractManyToManyChildThatIsAlsoParentCrudService<P extends ParentEntity<P>, C extends ChildEntityOf<P> & ParentEntity<C>> extends AbstractParentCrudService<C> {
    protected abstract EntityStore<P> getParentEntityStore();

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public void updateEntity(final @NotNull @Valid C updatedEntity,
                             @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id
    ) throws EntityNotFoundException {
        final C currentEntity = getEntityStore().readEntityWithId(id);
        final List<P> parentEntities = currentEntity.getParentEntities();
        final C newEntity;

        if (updatedEntity.hasChildEntities()) {
            newEntity = getEntityStore().readAndDetachEntityWithId(id);
            newEntity.updateChildEntitiesFromOtherParentEntity(updatedEntity);
            getEntityStore().updateEntity(newEntity);
            updatedEntity.removeAllChildEntities();
        } else {
            newEntity = getEntityStore().updateAndGetEntityWithId(id, updatedEntity);
        }

        for (final P parentEntity : parentEntities) {
            if (parentEntity != null) {
                parentEntity.replaceChildEntity(currentEntity, newEntity);
                getParentEntityStore().updateEntity(parentEntity);
            }
        }
    }

    @DELETE
    @Override
    public void deleteAllEntities(
            @DefaultValue(Constants.DEFAULT_NAME_FILTER) @QueryParam("nameFilter") final String nameFilter
    ) {
        final Iterable<C> entities = new ArrayList<>(getEntityStore().readEntities());
        for(final C entity : entities) {
            //noinspection ObjectAllocationInLoop
            final Iterable<P> parentEntities = new ArrayList<>(entity.getParentEntities());

            for (final P parentEntity : parentEntities) {
                if (parentEntity != null) {
                    parentEntity.removeChildEntity(entity);
                    getParentEntityStore().updateEntity(parentEntity);
                }
            }

            getEntityStore().deleteEntity(entity);
        }
    }

    @DELETE
    @Path("{id}")
    @Override
    public void deleteEntity(
            @PathParam("id") final @Min(value = 1, message = Constants.ID_MUST_BE_GREATER_THAN_ZERO) long id
    ) throws EntityNotFoundException {
        final C entity = getEntityStore().readEntityWithId(id);
        final Iterable<P> parentEntities = new ArrayList<>(entity.getParentEntities());

        for (final P parentEntity : parentEntities) {
            if (parentEntity != null) {
                parentEntity.removeChildEntity(entity);
                getParentEntityStore().updateEntity(parentEntity);
            }
        }

        getEntityStore().deleteEntity(entity);
    }
}
