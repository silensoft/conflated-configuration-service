package com.silensoft.conflated.configuration.service.selectedmeasure;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.chart.ChartStore;
import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("charts/{parentId}/selected-measures")
public class ChartSelectedMeasureService extends AbstractChildCrudServiceForParent<Chart, SelectedMeasure> {
    @Inject
    private SelectedMeasureStore selectedMeasureStore;

    @Inject
    private ChartStore chartStore;

    @Override
    protected EntityStore<SelectedMeasure> getChildEntityStore() {
        return selectedMeasureStore;
    }

    @Override
    protected EntityStore<Chart> getParentEntityStore() {
        return chartStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "chart";
    }
}
