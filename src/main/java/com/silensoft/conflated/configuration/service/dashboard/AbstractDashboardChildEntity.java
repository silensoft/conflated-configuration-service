package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.common.entity.AbstractIdentifiableEntity;
import com.silensoft.conflated.configuration.service.common.entity.ChildEntityOf;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Collections;
import java.util.List;

@MappedSuperclass
@SuppressWarnings({"InstanceVariableOfConcreteClass", "MethodParameterOfConcreteClass"})
public class AbstractDashboardChildEntity extends AbstractIdentifiableEntity implements ChildEntityOf<Dashboard> {
    @ManyToOne
    @JoinColumn(name = "dashboard_id")
    private Dashboard dashboard;

    @Override
    public void setParentEntity(final Dashboard newDashboard) {
        dashboard = newDashboard;
    }

    @JsonbTransient
    @Override
    public List<Dashboard> getParentEntities() {
        return Collections.singletonList(dashboard);
    }
}
