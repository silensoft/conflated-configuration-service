package com.silensoft.conflated.configuration.service.selecteddimension;

import com.silensoft.conflated.configuration.service.common.service.AbstractCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("selected-dimensions")
public class SelectedDimensionService extends AbstractCrudService<SelectedDimension> {
    @Inject
    private SelectedDimensionStore selectedDimensionStore;

    @Override
    protected EntityStore<SelectedDimension> getEntityStore() {
        return selectedDimensionStore;
    }
}
