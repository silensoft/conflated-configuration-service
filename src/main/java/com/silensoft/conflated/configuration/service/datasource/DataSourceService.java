package com.silensoft.conflated.configuration.service.datasource;

import com.silensoft.conflated.configuration.service.common.service.AbstractParentCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("data-sources")
public class DataSourceService extends AbstractParentCrudService<DataSource> {
    @Inject
    private DataSourceStore dataSourceStore;

    @Override
    protected EntityStore<DataSource> getEntityStore() {
        return dataSourceStore;
    }
}
