package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.chart.Chart;
import com.silensoft.conflated.configuration.service.chart.ChartStore;
import com.silensoft.conflated.configuration.service.common.service.AbstractChildCrudServiceForParent;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("charts/{parentId}/selected-sort-bys")
public class ChartSelectedSortByService extends AbstractChildCrudServiceForParent<Chart, SelectedSortBy> {
    @Inject
    private SelectedSortByStore selectedSortByStore;

    @Inject
    private ChartStore chartStore;

    @Override
    protected EntityStore<SelectedSortBy> getChildEntityStore() {
        return selectedSortByStore;
    }

    @Override
    protected EntityStore<Chart> getParentEntityStore() {
        return chartStore;
    }

    @Override
    protected String getParentEntitiesName() {
        return "chart";
    }
}
