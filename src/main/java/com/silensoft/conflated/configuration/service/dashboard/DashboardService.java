package com.silensoft.conflated.configuration.service.dashboard;

import com.silensoft.conflated.configuration.service.common.service.AbstractManyToManyChildThatIsAlsoParentCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroup;
import com.silensoft.conflated.configuration.service.dashboardgroup.DashboardGroupStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("dashboards")
public class DashboardService extends AbstractManyToManyChildThatIsAlsoParentCrudService<DashboardGroup, Dashboard> {
    @Inject
    private DashboardStore dashboardStore;

    @Inject
    private DashboardGroupStore dashboardGroupStore;

    @Override
    protected EntityStore<Dashboard> getEntityStore() {
        return dashboardStore;
    }

    @Override
    protected EntityStore<DashboardGroup> getParentEntityStore() {
        return dashboardGroupStore;
    }
}
