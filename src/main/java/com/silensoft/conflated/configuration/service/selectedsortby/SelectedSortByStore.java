package com.silensoft.conflated.configuration.service.selectedsortby;

import com.silensoft.conflated.configuration.service.common.store.EntityStore;

public interface SelectedSortByStore extends EntityStore<SelectedSortBy> {
}
