package com.silensoft.conflated.configuration.service.chart;

import com.silensoft.conflated.configuration.service.common.service.AbstractParentCrudService;
import com.silensoft.conflated.configuration.service.common.store.EntityStore;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

@Stateless
@Path("charts")
public class ChartService extends AbstractParentCrudService<Chart> {
    @Inject
    private ChartStore chartStore;

    @Override
    public EntityStore<Chart> getEntityStore() {
        return chartStore;
    }
}
